Locus Sonus Streambox Arch
==========================

* [Prepare your SD card]
* [Configure users]
* [Configure network access]
* [Install DarkIce]
* [Install the web server]
* [Add GPIO interface support]
* [Add Cirrus Logic/Wolfson Audio Card support]
* [Finalize]
* [Test your streambox]
* [Build Darkice from sources]
* [References]




Prepare your SD card
--------------------
Install ArchLinux ARM on SD card.  
Raspberry Pi  : http://archlinuxarm.org/platforms/armv6/raspberry-pi  
Raspberry Pi 2: http://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2  
Raspberry Pi 3: https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3  
Raspberry Pi 4: https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4

Find the RPI IP address, enter ssh as root and upgrade the system.  
*login: root/root*
``` bash
pacman -Syu
```








Configure users
---------------
Create a new user 'locusonus' as administrator (pass: *locusonus*).
``` bash
# create user
useradd -m -G wheel locusonus
# add password, enter `locusonus` at prompt
passwd locusonus
# add permission
chmod 755 /home/locusonus
# install sudo package
pacman -S sudo
# open sudoers file
EDITOR=nano visudo
# uncomment this line, save and exit
'%wheel ALL=(ALL) ALL'
```

Download and extract the streambox core files to the locusonus's home directory.
``` bash
curl http://locusonus.org/downloads/streambox/release/streambox-core-3.1.tar.gz | tar xvz -C /home/locusonus
chmod +x /home/locusonus/bin/*.sh
```







Configure network access
------------------------
Set the new hostname to *'streambox'*.
``` bash
hostnamectl set-hostname streambox
```


Install and configure the multicast Domain Name System or [mDNS](http://en.wikipedia.org/wiki/Multicast_DNS).
``` bash
# install packages
pacman -S avahi nss-mdns
# open the configuration file
nano /etc/nsswitch.conf
# change this line (if founded)
'hosts: files dns myhostname'
# to, save and exit
'hosts: files mdns_minimal [NOTFOUND=return] dns myhostname'
# copy ssh services
cp /home/locusonus/service/sftp-ssh.service /etc/avahi/services/
cp /home/locusonus/service/ssh.service /etc/avahi/services/
# enable the service on boot
systemctl enable avahi-daemon
```



Install dependencies for managing the wireless connection.
``` bash
pacman -S iw wireless_tools wpa_supplicant
```

Install dependencies for managing IOS USB tethering.
``` bash
pacman -S usbmuxd libimobiledevice ifuse
```

Install dependencies for managing the USB modem connection and pacth ppp connection script.
``` bash
# install packages
pacman -S usb_modeswitch ppp
<!--
# open the script file
nano /usr/lib/network/connections/mobile_ppp
# change this line
${AccessPointName:+'OK-AT-OK' 'AT+CGDCONT=1,"IP",$(quote_word "$AccessPointName")'}
# to, save and exit
${AccessPointName:+'OK-AT-OK' 'AT+CGDCONT=1,\"IP\",$(quote_word "$AccessPointName")'}
-->
```



Install dependencies to support remote access.
``` bash
pacman -S miniupnpc
```



Add Network services and enable the service on boot.  
**the streambox core files are required**, see [Configure users] section
``` bash
cp /home/locusonus/service/streambox-network.service /usr/lib/systemd/system
systemctl enable streambox-network
```








Install DarkIce
---------------
**The streambox core files are required**, see [Configure users] section  
To build Darkice from sources, see [the end of this documentation](#build-darkice-from-sources-optional).


Install Darkice 1.2.1 and audio utils.
``` bash
# for Raspberry Pi (armv6)
pacman -U /home/locusonus/darkice/darkice-1.2-1-armv6h.pkg.tar.xz
pacman -S alsa-utils alsa-plugins alsa-firmware alsa-lib
# for Raspberry Pi 2 (armv7)
pacman -U /home/locusonus/darkice/darkice-1.2-1-armv7h.pkg.tar.xz
pacman -S alsa-utils alsa-plugins alsa-firmware alsa-lib
```


Add DarkIce service and enable the service on boot.
``` bash
cp /home/locusonus/service/streambox-darkice.service /usr/lib/systemd/system
systemctl enable streambox-darkice
```

Notes: the DarkIce configuration file (darkice.cfg) will be downloaded to */boot* directory.


Create a virtual two-channel device
``` bash
cp /home/locusonus/conf/asound.conf /etc/asound.conf
``` 








Install the web server and configure
------------------------------------
Install the lighttpd + php packages from the official repositories.
``` bash
pacman -S lighttpd fcgi php-cgi
```


Configure the server.
``` bash
# open the server configuration file
nano /etc/lighttpd/lighttpd.conf
# change the default 'server.document-root' directory
'server.document-root    = "/home/locusonus/www"'
# add the following lines to allow lighttpd to response to cross-domain request.
# save and exit
setenv.add-response-header = ("Access-Control-Allow-Origin" => "*")
server.modules            += ("mod_setenv")
```



Update Lighttpd configuration for FastCGI.
``` bash
# create a new directory
mkdir /etc/lighttpd/conf.d

# create a configuration file for fastCGI
nano /etc/lighttpd/conf.d/fastcgi.conf
# add the following lines, save and exit
server.modules   += ("mod_fastcgi")
index-file.names += ("index.php")
fastcgi.server    = ( 
    ".php" => (
        "localhost" => ( 
            "bin-path" => "/usr/bin/php-cgi",
            "socket" => "/tmp/php-fastcgi.sock",
            "broken-scriptfilename" => "enable",
            "max-procs" => 4,
            "bin-environment" => (
                "PHP_FCGI_CHILDREN" => "16"
            )
        )
    )
)
# link the fastCGI conf file
echo 'include "conf.d/fastcgi.conf"' >> /etc/lighttpd/lighttpd.conf
```


Add permissions to http (lighttpd user).
``` bash
# open sudoers file
EDITOR=nano visudo
# add the following lines, save and exit
http ALL=NOPASSWD: /usr/bin/systemctl
http ALL=NOPASSWD: /usr/bin/poweroff
http ALL=NOPASSWD: /usr/bin/reboot
http ALL=NOPASSWD: /usr/bin/cp
http ALL=NOPASSWD: /usr/bin/arecord
http ALL=NOPASSWD: /usr/bin/aplay
http ALL=NOPASSWD: /usr/bin/iptables
http ALL=NOPASSWD: /home/locusonus/bin/audio.sh
http ALL=NOPASSWD: /home/locusonus/bin/network.sh
```


Modify some PHP configuration values.
``` bash
# open PHP configuration file
nano /etc/php/php.ini
# search for each configuration variable below (ctrl+W), modify, save and exit
# change 'error_reporting' to
'error_reporting = E_ALL & ~E_NOTICE & ~E_STRICT'
# Turn on the display of errors
'display_errors = On'
```

Set http account expiration date to 'never'
``` bash
chage -E -1 http
```

Add access to the default streambox config file.  
**The streambox core files are required**, see [Configure users] section.  
``` bash
chown -R http:http /home/locusonus/conf
```

Enable the web server service on boot.
``` bash
systemctl enable lighttpd
```








Add GPIO interface support
--------------------------
Install dependencies to interact with [LED status display](http://locusonus.org/streambox/statusled/)
(web and on Air).  
**The streambox core files are required**, see [Configure users] section

``` bash
pacman -S wiringpi
cp /home/locusonus/service/streambox-statusled.service /usr/lib/systemd/system
```








Add Cirrus Logic/Wolfson Audio Card support (optional)
------------------------------------------------------
Add repository named wsp to /etc/pacman.conf configuration file.
``` bash
# - open sudoers file
nano /etc/pacman.conf
# - add the following lines, save and exit
# - for Raspberry Pi (armv6)
# Cirrus Logic/Wolfson Audio Card: http://headless.audio/
[wsp]
SigLevel = Never
Server = http://headless.audio/pkg/armv6h
# - for Raspberry Pi 2 (armv7)
# Cirrus Logic/Wolfson Audio Card: http://headless.audio/
[wsp]
SigLevel = Never
Server = http://headless.audio/pkg/armv7h
```

Update packages database and install linux-raspberrypi-wsp package (linux-raspberrypi package replacement)
``` bash
pacman -Syu
pacman -S linux-raspberrypi-wsp
```

Enable onboard audio interface, Cirrus Logic/Wolfson card and mmap support.
mmap support is optional but required by JACK daemon.
``` bash
# open the config file
nano /boot/config.txt
# uncomment the follow lines, save and exit
dtparam=audio=on
dtoverlay=i2s-mmap
dtoverlay=rpi-cirrus-wm5102
```






Finalize
--------
**The streambox core files are required**, see [Configure users] section

Add statically the streambox initialization service.
``` bash
cp /home/locusonus/service/streambox-init.service /usr/lib/systemd/system
ln -s /usr/lib/systemd/system/streambox-init.service /usr/lib/systemd/system/sysinit.target.wants/
```

Enable the streambox monitoring.
``` bash
cp /home/locusonus/service/streambox-monitor.* /usr/lib/systemd/system
systemctl enable streambox-monitor.timer
```


Copy the empty streambox configuration file on boot for people who want to edit this file manually before starting the PI. Notes: this file will be overrided (and create if not exists) each time you save your streambox configuration from your web browser.
``` bash
cp /home/locusonus/conf/streambox /boot/streambox.txt
```

Turn power off.
``` bash
poweroff
```








Testing/Setup your streambox from browser
-----------------------------------------

After few seconds, turn power on and log in again.
Everythings should be setup / control from this adress: http://streambox.local/  
or at least using his IP address directly.

Notes: from an Androïd smartphone, connect the phone to your streambox using an USB cable. Enable the tethering option from your phone settings, and use a network scan application to find your streambox address.







Build Darkice from sources (optional)
-------------------------------------
Install compilation tools
``` bash
pacman -S gcc make fakeroot
```

Download, extract and build the package.
See the 'Install DarkIce' section to complete your installation.
``` bash
cd /home/locusonus
curl -O  http://sam.vis.nu/archlinux/darkice-1.2-1.src.tar.gz
pacman -S lame libpulse faac jack twolame opus libsamplerate
tar xzf darkice-1.2-1.src.tar.gz
cd darkice && makepkg
```







References
----------

http://elinux.org/ArchLinux_Install_Guide  
https://wiki.archlinux.org/index.php/Raspberry_Pi  
https://wiki.archlinux.org/index.php/Pacman_Rosetta (command line equivalent)  
https://wiki.archlinux.org/index.php/Systemd#Using_units  
https://wiki.archlinux.org/index.php/Wireless_network_configuration  
https://wiki.archlinux.org/index.php/IPhone_Tethering  
https://wiki.archlinux.org/index.php/USB_3G_Modem  
https://wiki.archlinux.org/index.php/lighttpd  
http://headless.audio/  






[top]: #locus-sonus-streambox-arch
[Prepare your SD card]: #prepare-your-sd-card
[Configure users]: #configure-users
[Configure network access]: #configure-network-access
[Install DarkIce]: #install-darkice
[Install the web server]: #install-the-web-server-and-configure
[Add GPIO interface support]: #add-gpio-interface-support
[Add Cirrus Logic/Wolfson Audio Card support]: #add-cirrus-logicwolfson-audio-card-support-optional
[Finalize]: #finalize
[Test your streambox]: #testingsetup-your-streambox-from-browser
[Build Darkice from sources]: #build-darkice-from-sources-optional
[References]: #references
