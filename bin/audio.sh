#!/usr/bin/sh

# 
# File: audiocard.sh
# Version: 0.2.1
# Authors: Stéphane Cousot <http://www.ubaa.net/>
# License: audiocard.sh is released under GNU General Public License, version 3 or later
#
#	- value to DB = ( value-(Math.abs(min)/step) )*step
# 	- DB to value = ( DB+Math.abs(min) ) / step
#
# --------------------------------------------------------------------------------------
#
# This file is part of the Locus Sonus streambox project.
# for more informations, see <http://locusonus.org/streambox/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



if [ `whoami` != 'root' ]; then # && [ `whoami` != 'http' ]; then
	echo "Permission denied"
	echo "you must be root to run this script."
	exit 1
fi



# load (copy first) configuration file
if [ ! -f /boot/streambox.txt ]; then
	cp /home/locusonus/conf/streambox /boot/streambox.txt
fi
source /boot/streambox.txt







docheck()
{
	case $@ in
		icicle )
			CARD="Icicle"
			NAME="Blue Icicle"
			;;
		cirrus )
			CARD="RPiCirrus"
			NAME="Cirrus Logic/Wolfson"
			;;
		* )
			usage
			exit 1
			;;
	esac


	CHECK=`arecord -l | grep "$CARD"`
	if [ "$CHECK" ]; then 
		echo "the $NAME audio card is connected"
	fi
}

dotest()
{
	if [ "$1" == "stop" ]; then 
		PROC=`ps x -o  "%r %c" | grep "audio.sh" | head -n 1`
		if [ "$PROC" ]; then 
			PGID=`echo -e "${PROC}" | sed -e 's/^[[:space:]]*//' | cut -d' ' -f1`
			if [ "$PGID" != "$$" ]; then kill -TERM -$PGID; fi
		fi
		exit 0
	fi

	NDEV=`arecord -l | grep "card" | wc -l`
	if [ "$SOUNDCARD" -gt "$NDEV" ]; then SOUNDCARD=`arecord -l | pcregrep -o1 -e "card (\d+)" | head -n 1`; fi
	if [[ "$VIRTUAL_STEREO" == "yes" && "$NDEV" != "1" ]]; then IN="virtual";
	else IN="hw:${SOUNDCARD:-1},0"; fi

	OUT=`aplay -l | pcregrep -o1 -e "card (\d+)" | tail -1`
	FORMAT=`arecord -D $IN --dump-hw-params 2>&1 | tail -1 | cut -d ' ' -f2`
	CHANNELS="$AUDIO_CHANNEL"

	# force DAC stereo only
	OUT_CHANNELS=`aplay -D hw:$OUT,0 --dump-hw-params -d1 /dev/zero 2>&1 | grep "CHANNELS"`
	if [[ -z `echo $OUT_CHANNELS | grep "$CHANNELS"` ]]; then CHANNELS=`echo $OUT_CHANNELS | pcregrep -o1 -e "CHANNELS.*(\d+)"`; fi

	arecord -D $IN -r $AUDIO_SAMPLERATE -c $CHANNELS -f $FORMAT | \
	aplay -D hw:$OUT,0 -r $AUDIO_SAMPLERATE -c $CHANNELS -f $FORMAT

}


dbToValue()
{ 
	DB="$1"
	MIN=`awk "BEGIN{x=$2; print x<0?-x:x}"`
	STEP="$3"

	if [[ -z $STEP ]]; then STEP="1"; fi
	awk "BEGIN {print ($DB+$MIN)/$STEP}"
}

valueToDB()
{

	VAL="$1"
	MIN=`awk "BEGIN{x=$2; print x<0?-x:x}"`
	STEP="$3"

	if [[ -z $STEP ]]; then STEP="1"; fi
	awk "BEGIN {print ($VAL-($MIN/$STEP))*$STEP}"
}

printDB()
{
	printf "%+.2f dB\n" $1 | sed 's/[+-]/\0 /'
}




# ---------------------------------------------------------------------------------------------------------------------
#	AUDIO CARD SETUP
# ---------------------------------------------------------------------------------------------------------------------

configure()
{
	HAVE_ICICLE=`docheck icicle`
	if [ "$HAVE_ICICLE" ]; then
		icicle_config -q
	fi

	HAVE_CIRRUS=`docheck cirrus`
	if [ "$HAVE_CIRRUS" ]; then
		cirrus_config -q
	fi
}




# ---------------------------------------------------------------------------------------------------------------------
#	BLUE ICILE AUDIO CARD
# ---------------------------------------------------------------------------------------------------------------------

doicicle()
{
	case $1 in
		setup )
			icicle_config
			;;
		reset )
			icicle_reset
			;;
		status )
			icicle_status
			;;
		* )
			usage
			exit 1
			;;
	esac
}

icicle_reset()
{
	if [ "$1" != "-q" ]; then 
		echo "-- Reseting the Blue Icicle audio card to 0 dB"
	fi

	amixer -q -Dhw:Icicle cset name='Mic Capture Volume' 8
}

icicle_config()
{
	VALUE=`dbToValue $BLUEICICLE_IN_GAIN -8`

	if [ "$1" != "-q" ]; then 
		echo "-- Configuring the Blue Icicle audio card"
		echo "-- set ALSA mixer to $VALUE ($BLUEICICLE_IN_GAIN dB)"
	fi

	amixer -q -Dhw:Icicle cset name='Mic Capture Volume' $VALUE
}

icicle_status()
{
	HAVE_ICICLE=`docheck icicle`
	if [[ -z "$HAVE_ICICLE" ]]; then
		echo "Blue Icicle audio card: device not found"
		exit 1
	fi

	echo "Blue Icicle audio card"
	echo -e "\ninput:" && echo "------"
	echo -n "mic capture volume ................ " && printDB `valueToDB $(icicle_in_get) -8`
}

icicle_in_get()
{
	echo `amixer -Dhw:Icicle cget name='Mic Capture Volume' | pcregrep -o1 -e ": values=(\d+)"`
}




# ---------------------------------------------------------------------------------------------------------------------
#	CIRRUS LOGIC / WOLFSON AUDIO CARD
# ---------------------------------------------------------------------------------------------------------------------

docirrus()
{
	case $1 in
		setup )
			cirrus_config
			;;
		reset )
			cirrus_reset
			;;
		check )
			cirrus_check $2
			;;
		status )
			cirrus_check all
			;;
		open )
			cirrus_open $2
			;;
		close )
			cirrus_close $2
			;;
		* )
			usage
			exit 1
			;;
	esac
}

cirrus_config()
{
	
	cirrus_reset $1

	if [ "$1" != "-q" ]; then 

		INL_VOL=`dbToValue $CIRRUS_INL_VOL -32`
		INR_VOL=`dbToValue $CIRRUS_INR_VOL -32`
		INL_PGA=`dbToValue $CIRRUS_INL_PGA 0`
		INR_PGA=`dbToValue $CIRRUS_INR_PGA 0`
		INL_DIG=`dbToValue $CIRRUS_INL_DIG -64 0.5`
		INR_DIG=`dbToValue $CIRRUS_INR_DIG -64 0.5`

		echo "-- Configuring the Cirrus Logic/Wolfson audio card for '$CIRRUS_INPUT' input"
		echo "-- Configuring the Cirrus Logic/Wolfson audio card for headphone ($CIRRUS_HEADPHONE) output"
		echo "-- Configuring the Cirrus Logic/Wolfson audio card for speaker ($CIRRUS_SPEAKER) output"
		echo "-- Configuring the Cirrus Logic/Wolfson audio card for lineout ($CIRRUS_LINE_OUT) output"
		echo "-- Configuring the Cirrus Logic/Wolfson audio card for spdifout ($CIRRUS_SPDIF_OUT) output"
		echo "-- set ALSA mixer to $INL_VOL ($CIRRUS_INL_VOL dB)"
		echo "-- set ALSA mixer to $INR_VOL ($CIRRUS_INR_VOL dB)"
		echo "-- set ALSA mixer to $INL_PGA ($CIRRUS_INL_PGA dB)"
		echo "-- set ALSA mixer to $INR_PGA ($CIRRUS_INR_PGA dB)"
		echo "-- set ALSA mixer to $INL_DIG ($CIRRUS_INL_DIG dB)"
		echo "-- set ALSA mixer to $INR_DIG ($CIRRUS_INR_DIG dB)"
	fi

	# input
	cirrus_open $CIRRUS_INPUT
		
	# output
	if [ "$CIRRUS_HEADPHONE" == "yes" ]; then cirrus_open headphone; fi
	if [ "$CIRRUS_SPEAKER" == "yes" ]; then cirrus_open speaker; fi
	if [ "$CIRRUS_LINE_OUT" == "yes" ]; then cirrus_open lineout; fi
	if [ "$CIRRUS_SPDIF_OUT" == "yes" ]; then cirrus_open spdifout; fi
}

cirrus_reset_in()
{
	# in + filers
	amixer -q -c RPiCirrus cset name="IN1R Digital Volume" "128"
	amixer -q -c RPiCirrus cset name="IN1R Volume" "0"
	amixer -q -c RPiCirrus cset name="IN1 High Performance Switch" "off"
	amixer -q -c RPiCirrus cset name="IN2L Digital Volume" "128"
	amixer -q -c RPiCirrus cset name="IN2R Digital Volume" "128"
	amixer -q -c RPiCirrus cset name="IN2L Volume" "0"
	amixer -q -c RPiCirrus cset name="IN2R Volume" "0"
	amixer -q -c RPiCirrus cset name="IN2 High Performance Switch" "off"
	amixer -q -c RPiCirrus cset name="IN3L Digital Volume" "128"
	amixer -q -c RPiCirrus cset name="IN3R Digital Volume" "128"
	amixer -q -c RPiCirrus cset name="IN3L Volume" "0"
	amixer -q -c RPiCirrus cset name="IN3R Volume" "0"
	amixer -q -c RPiCirrus cset name="IN3 High Performance Switch" "off"
	amixer -q -c RPiCirrus cset name="Line Input Micbias" "off"
	amixer -q -c RPiCirrus cset name="LHPF1 Coefficients" "0,0"
	amixer -q -c RPiCirrus cset name="LHPF2 Coefficients" "0,0"
	amixer -q -c RPiCirrus cset name="LHPF3 Coefficients" "0,0"
	amixer -q -c RPiCirrus cset name="LHPF4 Coefficients" "0,0"
	amixer -q -c RPiCirrus cset name="LHPF1 Input 1" "None"
	amixer -q -c RPiCirrus cset name="LHPF2 Input 1" "None"
	amixer -q -c RPiCirrus cset name="LHPF1 Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF2 Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF1 Input 2" "None"
	amixer -q -c RPiCirrus cset name="LHPF2 Input 2" "None"
	amixer -q -c RPiCirrus cset name="LHPF1 Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF2 Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF1 Input 3" "None"
	amixer -q -c RPiCirrus cset name="LHPF2 Input 3" "None"
	amixer -q -c RPiCirrus cset name="LHPF1 Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF2 Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF1 Input 4" "None"
	amixer -q -c RPiCirrus cset name="LHPF2 Input 4" "None"
	amixer -q -c RPiCirrus cset name="LHPF1 Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF2 Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF1TX1 Input 1" "None"
	amixer -q -c RPiCirrus cset name="AIF1TX2 Input 1" "None"
	amixer -q -c RPiCirrus cset name="AIF1TX1 Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF1TX2 Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF1TX1 Input 2" "None"
	amixer -q -c RPiCirrus cset name="AIF1TX2 Input 2" "None"
	amixer -q -c RPiCirrus cset name="AIF1TX1 Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF1TX2 Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF1TX1 Input 3" "None"
	amixer -q -c RPiCirrus cset name="AIF1TX2 Input 3" "None"
	amixer -q -c RPiCirrus cset name="AIF1TX1 Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF1TX2 Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF1TX1 Input 4" "None"
	amixer -q -c RPiCirrus cset name="AIF1TX2 Input 4" "None"
	amixer -q -c RPiCirrus cset name="AIF1TX1 Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF1TX2 Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF2TX1 Input 1" "None"
	amixer -q -c RPiCirrus cset name="AIF2TX2 Input 1" "None"
	amixer -q -c RPiCirrus cset name="AIF2TX1 Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF2TX2 Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF2TX1 Input 2" "None"
	amixer -q -c RPiCirrus cset name="AIF2TX2 Input 2" "None"
	amixer -q -c RPiCirrus cset name="AIF2TX1 Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF2TX2 Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF2TX1 Input 3" "None"
	amixer -q -c RPiCirrus cset name="AIF2TX2 Input 3" "None"
	amixer -q -c RPiCirrus cset name="AIF2TX1 Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF2TX2 Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF2TX1 Input 4" "None"
	amixer -q -c RPiCirrus cset name="AIF2TX2 Input 4" "None"
	amixer -q -c RPiCirrus cset name="AIF2TX1 Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="AIF2TX2 Input 4 Volume" "32"
}

cirrus_reset_out()
{
	# out
	amixer -q -c RPiCirrus cset name="HPOUT2 Digital Switch" "off"
	amixer -q -c RPiCirrus cset name="HPOUT2 Digital Volume" "128"
	amixer -q -c RPiCirrus cset name="HPOUT2L Input 1" "None"
	amixer -q -c RPiCirrus cset name="HPOUT2R Input 1" "None"
	amixer -q -c RPiCirrus cset name="HPOUT2L Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT2R Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT2L Input 2" "None"
	amixer -q -c RPiCirrus cset name="HPOUT2R Input 2" "None"
	amixer -q -c RPiCirrus cset name="HPOUT2L Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT2R Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT2L Input 3" "None"
	amixer -q -c RPiCirrus cset name="HPOUT2R Input 3" "None"
	amixer -q -c RPiCirrus cset name="HPOUT2L Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT2R Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT2L Input 4" "None"
	amixer -q -c RPiCirrus cset name="HPOUT2R Input 4" "None"
	amixer -q -c RPiCirrus cset name="HPOUT2L Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT2R Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT1 Digital Switch" "off"
	amixer -q -c RPiCirrus cset name="HPOUT1 Digital Volume" "128"
	amixer -q -c RPiCirrus cset name="HPOUT1L Input 1" "None"
	amixer -q -c RPiCirrus cset name="HPOUT1R Input 1" "None"
	amixer -q -c RPiCirrus cset name="HPOUT1L Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT1R Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT1L Input 2" "None"
	amixer -q -c RPiCirrus cset name="HPOUT1R Input 2" "None"
	amixer -q -c RPiCirrus cset name="HPOUT1L Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT1R Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT1L Input 3" "None"
	amixer -q -c RPiCirrus cset name="HPOUT1R Input 3" "None"
	amixer -q -c RPiCirrus cset name="HPOUT1L Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT1R Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT1L Input 4" "None"
	amixer -q -c RPiCirrus cset name="HPOUT1R Input 4" "None"
	amixer -q -c RPiCirrus cset name="HPOUT1L Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="HPOUT1R Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="Speaker Digital Switch" "off"
	amixer -q -c RPiCirrus cset name="Speaker Digital Volume" "128"
	amixer -q -c RPiCirrus cset name="SPKOUTL Input 1" "None"
	amixer -q -c RPiCirrus cset name="SPKOUTR Input 1" "None"
	amixer -q -c RPiCirrus cset name="SPKOUTL Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="SPKOUTR Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="SPKOUTL Input 2" "None"
	amixer -q -c RPiCirrus cset name="SPKOUTR Input 2" "None"
	amixer -q -c RPiCirrus cset name="SPKOUTL Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="SPKOUTR Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="SPKOUTL Input 3" "None"
	amixer -q -c RPiCirrus cset name="SPKOUTR Input 3" "None"
	amixer -q -c RPiCirrus cset name="SPKOUTL Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="SPKOUTR Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="SPKOUTL Input 4" "None"
	amixer -q -c RPiCirrus cset name="SPKOUTR Input 4" "None"
	amixer -q -c RPiCirrus cset name="SPKOUTL Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="SPKOUTR Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF3 Input 1" "None"
	amixer -q -c RPiCirrus cset name="LHPF4 Input 1" "None"
	amixer -q -c RPiCirrus cset name="LHPF3 Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF4 Input 1 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF3 Input 2" "None"
	amixer -q -c RPiCirrus cset name="LHPF4 Input 2" "None"
	amixer -q -c RPiCirrus cset name="LHPF3 Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF4 Input 2 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF3 Input 3" "None"
	amixer -q -c RPiCirrus cset name="LHPF4 Input 3" "None"
	amixer -q -c RPiCirrus cset name="LHPF3 Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF4 Input 3 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF3 Input 4" "None"
	amixer -q -c RPiCirrus cset name="LHPF4 Input 4" "None"
	amixer -q -c RPiCirrus cset name="LHPF3 Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="LHPF4 Input 4 Volume" "32"
	amixer -q -c RPiCirrus cset name="Min Sample Rate" "off"
	amixer -q -c RPiCirrus cset name="Max Sample Rate" "off"
	amixer -q -c RPiCirrus cset name="Tx Source" "AIF"
}

cirrus_reset()
{
	if [ "$1" != "-q" ]; then 
		echo "-- Reseting the Cirrus Logic/Wolfson audio card to default path"
	fi

	cirrus_reset_in
	cirrus_reset_out
}

cirrus_check()
{
	HAVE_CIRRUS=`docheck cirrus`
	if [[ -z "$HAVE_CIRRUS" ]]; then
		echo "Cirrus Logic/Wolfson audio card: device not found"
		exit 1
	fi

	case $@ in
		mic       ) SWITCH="IN1 High Performance" ;;
		dmic      ) SWITCH="IN2 High Performance" ;;
		micbias   ) SWITCH="Line Input Micbias" ;;
		linein    ) SWITCH="IN3 High Performance" ;;
		spdifin   ) SWITCH="AIF1TX1 Input 1" ;;
		speaker   ) SWITCH="Speaker Digital" ;;
		headphone ) SWITCH="HPOUT1 Digital" ;;
		lineout   ) SWITCH="HPOUT2 Digital" ;;
		spdifout  ) SWITCH="AIF2TX1 Input 1" ;;
		all )

			case $CIRRUS_INPUT in
				mic )
					PGA=1
					DIG=1
					;;
				dmic )
					PGA=2
					DIG=2
					;;
				* )
					PGA=3
					DIG=3
					;;
			esac

			INL_VOL=`valueToDB $(cirrus_in_get "AIF1TX1 Input 1 Volume") -32`
			INR_VOL=`valueToDB $(cirrus_in_get "AIF1TX2 Input 1 Volume") -32`
			INL_PGA=`valueToDB $(cirrus_in_get "IN${PGA}L Volume") 0`
			INR_PGA=`valueToDB $(cirrus_in_get "IN${PGA}R Volume") 0`
			INL_DIG=`valueToDB $(cirrus_in_get "IN${DIG}L Digital Volume") -64 0.5`
			INR_DIG=`valueToDB $(cirrus_in_get "IN${DIG}R Digital Volume") -64 0.5`


			echo "Cirrus Logic/Wolfson audio card"
			echo -e "\ninput:" && echo "------"
			echo -n "Digital MEMS microphones (DMIC) ... " && cirrus_check dmic
			echo -n "Mic (HeadSet) ..................... " && cirrus_check mic
			echo -n "Line In  .......................... " && cirrus_check linein
			echo -n "Line In (bias voltage) ............ " && cirrus_check micbias
			echo -n "SPDIF In .......................... " && cirrus_check spdifin
			echo -e ""
			echo -n "audio interface volume (L) ........ " && printDB $INL_VOL
			echo -n "audio interface volume (R) ........ " && printDB $INR_VOL
			echo -n "input PGA gain (L) ................ " && printDB $INL_PGA
			echo -n "input PGA gain (R) ................ " && printDB $INR_PGA
			echo -n "digital input gain (L) ............ " && printDB $INL_DIG
			echo -n "digital input gain (R) ............ " && printDB $INR_DIG
			echo -e "\noutput:" && echo "-------"
			echo -n "Headphone  ........................ " && cirrus_check headphone
			echo -n "Speakers  ......................... " && cirrus_check speaker
			echo -n "Line Out .......................... " && cirrus_check lineout
			echo -n "SPDIF Out ......................... " && cirrus_check spdifout
			exit 0
			;;
		* )
			usage
			exit 1
			;;
	esac

	# spdifin : #24 'AIF2RX1', spdifout : #16 'AIF1RX1'
	ACTIVE=`amixer -c RPiCirrus cget name="$SWITCH" | grep -E ": values=on|: values=1|: values=16|: values=24"`

	if [[ "$1" == "linein" && `cirrus_check micbias` == "ON" ]]; then ACTIVE=""; fi
	if [ "$ACTIVE" ]; then echo "ON"; else echo "OFF"; fi
}

# cirrus_open()
# {
# 	case $@ in
# 		mic       ) SCRIPT_NAME="Record_from_Headset" ;;
# 		dmic      ) SCRIPT_NAME="Record_from_DMIC" ;;
# 		micbias   ) SCRIPT_NAME="Record_from_lineIn_Micbias" ;;
# 		linein    ) SCRIPT_NAME="Record_from_lineIn" ;;
# 		spdifin   ) SCRIPT_NAME="SPDIF_record" ;;
# 		speaker   ) SCRIPT_NAME="Playback_to_Speakers" ;;
# 		headphone ) SCRIPT_NAME="Playback_to_Headset" ;;
# 		lineout   ) SCRIPT_NAME="Playback_to_Lineout" ;;
# 		spdifout  ) SCRIPT_NAME="SPDIF_playback" ;;
# 		* )
# 			usage
# 			exit 1
# 			;;
# 	esac

# 	# adjust mixers
# 	#/home/locusonus/bin/$SCRIPT_NAME.sh -q
# }
cirrus_open()
{

	if [ -z "$CIRRUS_INL_VOL" ]; then CIRRUS_INL_VOL="0"; fi
	if [ -z "$CIRRUS_INR_VOL" ]; then CIRRUS_INR_VOL="0"; fi
	if [ -z "$CIRRUS_INL_PGA" ]; then CIRRUS_INL_PGA="0"; fi
	if [ -z "$CIRRUS_INR_PGA" ]; then CIRRUS_INR_PGA="0"; fi
	if [ -z "$CIRRUS_INL_DIG" ]; then CIRRUS_INL_DIG="-6"; fi
	if [ -z "$CIRRUS_INR_DIG" ]; then CIRRUS_INR_DIG="-6"; fi
	if [ -z "$CIRRUS_OUTL_VOL" ]; then CIRRUS_OUTL_VOL="0"; fi
	if [ -z "$CIRRUS_OUTR_VOL" ]; then CIRRUS_OUTR_VOL="0"; fi


	case $@ in
		# Record_from_Headset.sh
		mic ) 
			cirrus_reset_in
			amixer -q -c RPiCirrus cset name='IN1R Volume' `dbToValue $CIRRUS_INR_PGA 0`
			amixer -q -c RPiCirrus cset name='IN1R Digital Volume' `dbToValue $CIRRUS_INR_DIG -64 0.5`
			amixer -q -c RPiCirrus cset name="LHPF1 Mode" High-pass
			amixer -q -c RPiCirrus cset name="LHPF2 Mode" High-pass
			amixer -q -c RPiCirrus cset name="LHPF1 Coefficients" 240,3
			amixer -q -c RPiCirrus cset name="LHPF2 Coefficients" 240,3
			amixer -q -c RPiCirrus cset name='LHPF1 Input 1' IN1R
			amixer -q -c RPiCirrus cset name="LHPF2 Input 1" IN1R
			amixer -q -c RPiCirrus cset name="LHPF1 Input 1 Volume" 32
			amixer -q -c RPiCirrus cset name="LHPF2 Input 1 Volume" 32
			amixer -q -c RPiCirrus cset name='AIF1TX1 Input 1' LHPF1
			amixer -q -c RPiCirrus cset name='AIF1TX2 Input 1' LHPF2
			amixer -q -c RPiCirrus cset name='AIF1TX1 Input 1 Volume' `dbToValue $CIRRUS_INL_VOL -32`
			amixer -q -c RPiCirrus cset name='AIF1TX2 Input 1 Volume' `dbToValue $CIRRUS_INR_VOL -32`
			amixer -q -c RPiCirrus cset name='IN1 High Performance Switch' on
			;;
		# Record_from_DMIC.sh
		dmic )
			cirrus_reset_in
			amixer -q -c RPiCirrus cset name="IN2L Volume" 0
			amixer -q -c RPiCirrus cset name="IN2R Volume" 0
			amixer -q -c RPiCirrus cset name='IN2L Digital Volume' `dbToValue $CIRRUS_INL_DIG -64 0.5`
			amixer -q -c RPiCirrus cset name='IN2R Digital Volume' `dbToValue $CIRRUS_INR_DIG -64 0.5`
			amixer -q -c RPiCirrus cset name='LHPF1 Input 1' IN2L
			amixer -q -c RPiCirrus cset name='LHPF2 Input 1' IN2R
			amixer -q -c RPiCirrus cset name='LHPF1 Mode' High-pass
			amixer -q -c RPiCirrus cset name='LHPF2 Mode' High-pass
			amixer -q -c RPiCirrus cset name='LHPF1 Coefficients' 240,3
			amixer -q -c RPiCirrus cset name='LHPF2 Coefficients' 240,3
			amixer -q -c RPiCirrus cset name="LHPF1 Input 1 Volume" 32
			amixer -q -c RPiCirrus cset name="LHPF2 Input 1 Volume" 32
			amixer -q -c RPiCirrus cset name='AIF1TX1 Input 1' LHPF1
			amixer -q -c RPiCirrus cset name='AIF1TX2 Input 1' LHPF2
			amixer -q -c RPiCirrus cset name='AIF1TX1 Input 1 Volume' `dbToValue $CIRRUS_INL_VOL -32`
			amixer -q -c RPiCirrus cset name='AIF1TX2 Input 1 Volume' `dbToValue $CIRRUS_INR_VOL -32`
			amixer -q -c RPiCirrus cset name='IN2 High Performance Switch' on
			;;
		# Record_from_lineIn_Micbias.sh
		micbias )
			cirrus_reset_in
			amixer -q -c RPiCirrus cset name="Line Input Micbias" on
			amixer -q -c RPiCirrus cset name="IN3 High Performance Switch" on
			amixer -q -c RPiCirrus cset name="IN3L Volume" `dbToValue $CIRRUS_INL_PGA 0`
			amixer -q -c RPiCirrus cset name="IN3R Volume" `dbToValue $CIRRUS_INR_PGA 0`
			amixer -q -c RPiCirrus cset name="IN3L Digital Volume" `dbToValue $CIRRUS_INL_DIG -64 0.5`
			amixer -q -c RPiCirrus cset name="IN3R Digital Volume" `dbToValue $CIRRUS_INR_DIG -64 0.5`
			amixer -q -c RPiCirrus cset name="LHPF1 Mode" High-pass
			amixer -q -c RPiCirrus cset name="LHPF2 Mode" High-pass
			amixer -q -c RPiCirrus cset name="LHPF1 Coefficients" 240,3
			amixer -q -c RPiCirrus cset name="LHPF2 Coefficients" 240,3
			amixer -q -c RPiCirrus cset name="LHPF1 Input 1" IN3L
			amixer -q -c RPiCirrus cset name="LHPF2 Input 1" IN3R
			amixer -q -c RPiCirrus cset name="LHPF1 Input 1 Volume" 32
			amixer -q -c RPiCirrus cset name="LHPF2 Input 1 Volume" 32
			amixer -q -c RPiCirrus cset name="AIF1TX1 Input 1" LHPF1
			amixer -q -c RPiCirrus cset name="AIF1TX2 Input 1" LHPF2
			amixer -q -c RPiCirrus cset name='AIF1TX1 Input 1 Volume' `dbToValue $CIRRUS_INL_VOL -32`
			amixer -q -c RPiCirrus cset name='AIF1TX2 Input 1 Volume' `dbToValue $CIRRUS_INR_VOL -32`
			;;
		# Record_from_lineIn.sh
		linein )
			cirrus_reset_in
			amixer -q -c RPiCirrus cset name="IN3 High Performance Switch" on
			amixer -q -c RPiCirrus cset name='IN3L Volume' `dbToValue $CIRRUS_INL_PGA 0`
			amixer -q -c RPiCirrus cset name='IN3R Volume' `dbToValue $CIRRUS_INR_PGA 0`
			amixer -q -c RPiCirrus cset name='IN3L Digital Volume' `dbToValue $CIRRUS_INL_DIG -64 0.5`
			amixer -q -c RPiCirrus cset name='IN3R Digital Volume' `dbToValue $CIRRUS_INR_DIG -64 0.5`
			amixer -q -c RPiCirrus cset name="LHPF1 Mode" High-pass
			amixer -q -c RPiCirrus cset name="LHPF2 Mode" High-pass
			amixer -q -c RPiCirrus cset name="LHPF1 Coefficients" 240,3
			amixer -q -c RPiCirrus cset name="LHPF2 Coefficients" 240,3
			amixer -q -c RPiCirrus cset name="LHPF1 Input 1" IN3L
			amixer -q -c RPiCirrus cset name="LHPF2 Input 1" IN3R
			amixer -q -c RPiCirrus cset name="LHPF1 Input 1 Volume" 32
			amixer -q -c RPiCirrus cset name="LHPF2 Input 1 Volume" 32
			amixer -q -c RPiCirrus cset name="AIF1TX1 Input 1" LHPF1
			amixer -q -c RPiCirrus cset name="AIF1TX2 Input 1" LHPF2
			amixer -q -c RPiCirrus cset name='AIF1TX1 Input 1 Volume' `dbToValue $CIRRUS_INL_VOL -32`
			amixer -q -c RPiCirrus cset name='AIF1TX2 Input 1 Volume' `dbToValue $CIRRUS_INR_VOL -32`
			;;
		# Record_from_SPDIF.sh
		spdifin )
			amixer -q -c RPiCirrus cset name="Min Sample Rate" 32kHz
			amixer -q -c RPiCirrus cset name="AIF1TX1 Input 1" AIF2RX1
			amixer -q -c RPiCirrus cset name='AIF1TX1 Input 1 Volume' `dbToValue $CIRRUS_INL_VOL -32`
			amixer -q -c RPiCirrus cset name="AIF1TX2 Input 1" AIF2RX2
			amixer -q -c RPiCirrus cset name='AIF1TX2 Input 1 Volume' `dbToValue $CIRRUS_INR_VOL -32`
			;;
		# Playback_to_Speakers.sh
		speaker )
			amixer -q -c RPiCirrus cset name='Speaker Digital Volume' 128
			amixer -q -c RPiCirrus cset name='SPKOUTL Input 1' None
			amixer -q -c RPiCirrus cset name='SPKOUTR Input 1' None
			amixer -q -c RPiCirrus cset name='SPKOUTL Input 2' None
			amixer -q -c RPiCirrus cset name='SPKOUTR Input 2' None
			amixer -q -c RPiCirrus cset name='SPKOUTL Input 1' AIF1RX1
			amixer -q -c RPiCirrus cset name='SPKOUTR Input 1' AIF1RX2
			amixer -q -c RPiCirrus cset name='SPKOUTL Input 1 Volume' `dbToValue $CIRRUS_OUTL_VOL -32`
			amixer -q -c RPiCirrus cset name='SPKOUTR Input 1 Volume' `dbToValue $CIRRUS_OUTR_VOL -32`
			amixer -q -c RPiCirrus cset name='Speaker Digital Switch' on
			;;
		# Playback_to_Headset.sh
		headphone )
			amixer -q -c RPiCirrus cset name='HPOUT1 Digital Volume' 116
			amixer -q -c RPiCirrus cset name='HPOUT1L Input 1' None
			amixer -q -c RPiCirrus cset name='HPOUT1R Input 1' None
			amixer -q -c RPiCirrus cset name='HPOUT1L Input 2' None
			amixer -q -c RPiCirrus cset name='HPOUT1R Input 2' None
			amixer -q -c RPiCirrus cset name='HPOUT1L Input 1' AIF1RX1
			amixer -q -c RPiCirrus cset name='HPOUT1R Input 1' AIF1RX2
			amixer -q -c RPiCirrus cset name='HPOUT1L Input 1 Volume' `dbToValue $CIRRUS_OUTL_VOL -32`
			amixer -q -c RPiCirrus cset name='HPOUT1R Input 1 Volume' `dbToValue $CIRRUS_OUTR_VOL -32`
			amixer -q -c RPiCirrus cset name='HPOUT1 Digital Switch' on
			;;
		# Playback_to_Lineout.sh
		lineout )
			amixer -q -c RPiCirrus cset name='HPOUT2L Input 1' None
			amixer -q -c RPiCirrus cset name='HPOUT2R Input 1' None
			amixer -q -c RPiCirrus cset name='HPOUT2L Input 2' None
			amixer -q -c RPiCirrus cset name='HPOUT2R Input 2' None
			amixer -q -c RPiCirrus cset name='HPOUT2L Input 1' AIF1RX1
			amixer -q -c RPiCirrus cset name='HPOUT2R Input 1' AIF1RX2
			amixer -q -c RPiCirrus cset name='HPOUT1L Input 1 Volume' `dbToValue $CIRRUS_OUTL_VOL -32`
			amixer -q -c RPiCirrus cset name='HPOUT1R Input 1 Volume' `dbToValue $CIRRUS_OUTR_VOL -32`
			amixer -q -c RPiCirrus cset name='HPOUT2 Digital Switch' on
			;;
		# Playback_to_SPDIF.sh
		spdifout )
			amixer -q -c RPiCirrus cset name="Min Sample Rate" 32kHz
			amixer -q -c RPiCirrus cset name='Tx Source' AIF
			amixer -q -c RPiCirrus cset name='AIF2TX1 Input 1' AIF1RX1
			amixer -q -c RPiCirrus cset name='AIF2TX2 Input 1' AIF1RX2
			amixer -q -c RPiCirrus cset name='AIF2TX1 Input 1 Volume' `dbToValue $CIRRUS_OUTL_VOL -32`
			amixer -q -c RPiCirrus cset name='AIF2TX2 Input 1 Volume' `dbToValue $CIRRUS_OUTR_VOL -32`
			;;
		* )
			usage
			exit 1
			;;
	esac
}

cirrus_close()
{
	case $@ in
		mic       )
			SWITCH="IN1 High Performance"
			OUTPUT="AIF"
			;;
		dmic      )
			SWITCH="IN2 High Performance"
			OUTPUT="AIF"
			;;
		micbias   )
			SWITCH="Line Input Micbias"
			OUTPUT="AIF"
			;;
		linein    )
			SWITCH="IN3 High Performance"
			OUTPUT="AIF"
			;;
		spdifin   )
			SWITCH="SPDIF In Switch"
			OUTPUT="AIF"
			;;
		speaker   )
			SWITCH="Speaker Digital"
			OUTPUT="SPK"
			;;
		headphone )
			SWITCH="HPOUT1 Digita"
			OUTPUT="HPOUT1"
			;;
		lineout   )
			SWITCH="HPOUT2 Digital"
			OUTPUT="HPOUT2"
			;;
		spdifout  )
			SWITCH="SPDIF Out"
			OUTPUT="AIF"
			;;
		* )
			usage
			exit 1
			;;
	esac

	if [ "$SWITCH" ]; then
		amixer -q -c RPiCirrus cset name="$SWITCH" off
	fi
	if [ "$OUTPUT" == "AIF" ]; then
		amixer -q -c RPiCirrus cset name='AIF2TX1 Input 1' None
		amixer -q -c RPiCirrus cset name='AIF2TX2 Input 1' None
		amixer -q -c RPiCirrus cset name='AIF1TX1 Input 1' None
		amixer -q -c RPiCirrus cset name='AIF1TX2 Input 1' None
	fi
	if [ "$OUTPUT" == "HPOUT1" ]; then
		amixer -q -c RPiCirrus cset name='HPOUT1L Input 1' None
		amixer -q -c RPiCirrus cset name='HPOUT1R Input 1' None
		amixer -q -c RPiCirrus cset name='HPOUT1L Input 2' None
		amixer -q -c RPiCirrus cset name='HPOUT1R Input 2' None
	fi
	if [ "$OUTPUT" == "HPOUT2" ]; then
		amixer -q -c RPiCirrus cset name='HPOUT2L Input 1' None
		amixer -q -c RPiCirrus cset name='HPOUT2R Input 1' None
		amixer -q -c RPiCirrus cset name='HPOUT2L Input 2' None
		amixer -q -c RPiCirrus cset name='HPOUT2R Input 2' None
	fi
	if [ "$OUTPUT" == "SPK" ]; then
		amixer -q -c RPiCirrus cset name='SPKOUTL Input 1' None
		amixer -q -c RPiCirrus cset name='SPKOUTR Input 1' None
		amixer -q -c RPiCirrus cset name='SPKOUTL Input 2' None
		amixer -q -c RPiCirrus cset name='SPKOUTR Input 2' None
	fi
}

cirrus_in_get()
{
	echo `amixer -c RPiCirrus cget name="$1" | pcregrep -o1 -e ": values=(\d+)"`
}




# ---------------------------------------------------------------------------------------------------------------------
#	MAIN FUNCTIONS
# ---------------------------------------------------------------------------------------------------------------------

usage()
{
	echo "$0 {COMMAND} ..."
	echo ""
	echo "Audio card manager"
	echo "Supported audio card: Blue Icicle, Cirrus Logic/Wolfson"
	echo 
	echo "  -h --help                         show this help"
	echo "  --test start|stop                 test direct input/output for the connected audio card"
	echo "  --check cirrus|icicle             test if the give audio card is connected"
	echo "  --setup                           configure the user (or default) requirements for all connected audio"
	echo "                                    board and add ALSA mixer settings"
	echo "  --icicle setup|reset              configure or reset the Blue Icicle audio card"
	echo "  --icicle status                   dump input status"
	echo "  --cirrus setup|reset              configure or reset the Cirrus Logic/Wolfson audio card"
	echo "  --cirrus status                   dump input or output status"
	echo "  --cirrus check|open|close in|out  check or manage the given Cirrus Logic/Wolfson input or output"
	echo "                                    where in is: dmic, mic, micbias, linein or spdifin"
	echo "                                    where out is: speaker, headphone, lineout or spdifout"
}

case $1 in
	--setup )
		configure
		;;
	--check )
		docheck $2
		;;
	--test )
		dotest $2
		;;
	--icicle )
		doicicle $2
		;;
	--cirrus )
		docirrus $2 $3
		;;
	-h|* )
		usage
		exit 1
		;;
esac
exit $?
