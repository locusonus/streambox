#!/usr/bin/sh

# 
# File: darkice.sh
# Version: 0.2
# Authors: Stéphane Cousot <http://www.ubaa.net/>
# License: darkice.sh is released under GNU General Public License, version 3 or later
#
# --------------------------------------------------------------------------------------
#
# This file is part of the Locus Sonus streambox project.
# for more informations, see <http://locusonus.org/streambox/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#




#set -e
source /boot/streambox.txt


DARKCONF="/boot/darkice.cfg"



if [ `whoami` != 'root' ]; then
	echo "Permission denied"
	echo "you must be root to run this script."
	exit 1
fi


run()
{

	# wait for a fully network configuration (eth* or wlan*)
	IP=`hostname -i`
	if [[ $IP = 127.0.0.* ]]; then
		echo "Waiting for the network to be configured..."
		while [[ $IP = 127.0.0.* ]]
		do
			IP=`hostname -i`
			sleep 2
		done
	fi



	# download configuration
	echo "Local ip address: $IP"
	echo "Download darkice config for '$LOGIN'"
	curl -# http://locusonus.org/soundmap/admin/autoconfig?usr=${LOGIN}\&pass=${PASSWORD}\&out=ini\&dev=rpi -o $DARKCONF

	# start darkice
	if [ -f $DARKCONF ]; then


		# use OSS instead of the default ALSA
		# or choose the selected user input
		if [ -f /dev/dsp ]; then
			perl -i.bak -pe 's/device.*/device          = \/dev\/dsp/g' $DARKCONF
		else
			NDEV=`arecord -l | grep "card" | wc -l`
			if [[ "$VIRTUAL_STEREO" == "yes" && "$NDEV" != "1" ]]; then 
				perl -i.bak -pe 's/device.*/device          = virtual/g' $DARKCONF
			else
				# if soundcard number is greater than the number of devices
				# use the first card found
				if [ "$SOUNDCARD" -gt "$NDEV" ]; then 
					SOUNDCARD=`arecord -l | pcregrep -o1 -e "card (\d+)" | head -n 1`
				fi
				export SOUNDCARD=${SOUNDCARD:-1}
				perl -i.bak -pe 's/device.*/device          = plughw:$ENV{"SOUNDCARD"},0/g' $DARKCONF
			fi
		fi

		# override audio configuration
		export AUDIO_CHANNEL=${AUDIO_CHANNEL:-1}
		export AUDIO_QUALITY=${AUDIO_QUALITY:-0.3}
		export AUDIO_SAMPLERATE=${AUDIO_SAMPLERATE:-44100}
		perl -i.bak -pe 's/channel.*/channel         = $ENV{"AUDIO_CHANNEL"}/g' $DARKCONF
		perl -i.bak -pe 's/quality.*/quality         = $ENV{"AUDIO_QUALITY"}/g' $DARKCONF
		perl -i.bak -pe 's/sampleRate.*/sampleRate      = $ENV{"AUDIO_SAMPLERATE"}/g' $DARKCONF


		# add desc: could be usefull ?
		export OS="archlinux `uname -m` - darkice 1.2.1"
		perl -i.bak -pe 's/description.*/$& - $ENV{"OS"}/g' $DARKCONF
		rm $DARKCONF.bak

		# run
		/usr/bin/darkice -c $DARKCONF

	fi

}


check()
{
	# restart if running but not mounted on server
	# note running means: status could be 'active' or 'failed'
	if [ `systemctl is-active streambox-darkice` != "inactive" ]; then
		/home/locusonus/bin/statusled -s 2>&1
		if [ $? != 0 ]; then
			echo "reload stream"
			systemctl restart streambox-darkice
		fi
	fi
}


case $1 in
	--check )
		check
		;;
	* )
		run
		;;
esac
exit $?

