#!/usr/bin/sh

# 
# File: network.sh
# Version: 0.2
# Authors: Stéphane Cousot <http://www.ubaa.net/>
# License: network.sh is released under GNU General Public License, version 3 or later
#
# --------------------------------------------------------------------------------------
#
# This file is part of the Locus Sonus streambox project.
# for more informations, see <http://locusonus.org/streambox/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



if [ `whoami` != 'root' ]; then # && [ `whoami` != 'http' ]; then
	echo "Permission denied"
	echo "you must be root to run this script."
	exit 1
fi


DEFAULT_HOSTANME="streambox"
WIFI_PROFILE="/etc/netctl/wlan"
MODEM_PROFILE="/etc/netctl/modem"
ETHERNET_PROFILE="/etc/systemd/network/eth1.network"
USBTETHERING_PROFILE="/etc/systemd/network/usb0.network"


# load (copy first) configuration file
if [ ! -f /boot/streambox.txt ]; then
	cp /home/locusonus/conf/streambox /boot/streambox.txt
fi
source /boot/streambox.txt




# ---------------------------------------------------------------------------------------------------------------------
#	HOSTNAME
# ---------------------------------------------------------------------------------------------------------------------

# change hostname
sethostname()
{

	if [ ! "$HOSTNAME" ]; then
		echo "Invalid hostname, set to the defaut '$DEFAULT_HOSTANME'"
		HOSTNAME=$DEFAULT_HOSTANME
	fi
	if [ `hostname` != `echo ${HOSTNAME,,}` ]; then
		echo "Set hostname to '$HOSTNAME'"
		hostnamectl set-hostname $HOSTNAME
		echo "Reboot" 
		sudo systemctl reboot -i
	else
		echo "Hostname already set to '$HOSTNAME'" 
	fi
}




# ---------------------------------------------------------------------------------------------------------------------
#	WIFI
#	https://wiki.archlinux.org/index.php/Wireless_network_configuration
# ---------------------------------------------------------------------------------------------------------------------

dowifi()
{
	case $@ in
		start )
			wifi_start
			;;
		stop )
			wifi_stop
			;;
		scan )
			wifi_scan
			;;
		status )
			wifi_status
			;;
		write )
			wifi_profile
			;;
		check )
			wifi_check
			;;
		* )
			usage
			exit 1
			;;
	esac
}


# check for Wireless interface
# https://wiki.archlinux.org/index.php/Wireless_network_configuration#Manual_setup
wifi_check()
{

	IWDEV=`iw dev`								# only supports the nl80211 (netlink) standard
	IWCNF=`iwconfig wlan0 2>&1 | grep wlan0`	# support the older WEXT (Wireless EXTentions) standard
	
	if [ "$IWDEV" == "" ] && [ "$IWCNF" == "wlan0     No such device" ]; then
		echo "Error: no Wireless interface found"
	 	exit 1
	fi

}

# start Wireless connection
wifi_start()
{
	
	# check interfaces first
	# and exit if not found
	wifi_check

	# check for SSID
	# and exit if not found
	if [ "$WIFI_SSID" == "" ]; then
		echo "Error: Incorrect wireless settings: missing SSID"
		exit 1
	fi

	# write profile
	# always write profile to match user manual modifications
	#if [ ! -f $WIFI_PROFILE ]; then
	wifi_profile
	#fi
	
	# start profile
	echo "Start wireless connection"
	ip link set wlan0 down
	netctl start `basename $WIFI_PROFILE`

}

# stop Wireless connection
wifi_stop()
{	
	# check interfaces first
	# and exit if not found
	wifi_check

	echo "Stop wireless connection"
	netctl stop `basename $WIFI_PROFILE`
}

# write Wireless profile
# https://wiki.archlinux.org/index.php/Wireless_network_configuration
wifi_profile()
{
	# # manual setup
	# # no encryption
	# if [ "$WIFI_SECURITY" == "none" ]; then
	# 	echo "-- use open connection"
	# 	iw dev wlan0 connect $WIFI_KEY
	# fi
	# # WEP
	# if [ "$WIFI_SECURITY" == "wep" ]; then
	# 	echo "-- use WEP encryption"
	# 	iw dev wlan0 connect $WIFI_SSID key 0:$WIFI_KEY
	# fi
	# # WPA/WPA2 
	# if [ "$WIFI_SECURITY" == "wpa" ]; then
	# 	echo "-- use WPA/WPA2 encryption"
	# 	wpa_passphrase $WIFI_SSID $WIFI_KEY > /tmp/wifi-conf
	# 	wpa_supplicant -i wlan0 -c /tmp/wifi-conf
	# fi


	# automatic setup using netctl
	# man netctl.profile for more informations

	# prepend \" for hexadecimal keys
	if [ "$WIFI_KEY_IS_HEXADECIMAL" == "yes" ]; then
		WIFI_KEY=\\\"$WIFI_KEY
	fi

	echo "Write wireless profile"
	cat << EOF > $WIFI_PROFILE
Description="Streambox wireless connection"
Interface=wlan0
Connection=wireless
Security=$WIFI_SECURITY
ESSID="$WIFI_SSID"
IP=dhcp
Key=$WIFI_KEY
Hidden=$WIFI_SSID_IS_HIDDEN
EOF
}

# scan for Wireless networks
wifi_scan()
{
	# check interfaces first
	# and exit if not found
	wifi_check

	ip link set wlan0 up

	echo "Scanning for networks ..."
	echo ""

	# nl80211 (netlink) standard
	IWDEV=`iw dev`
	if [ "$IWDEV" ]; then
		iw dev wlan0 scan | grep SSID | perl -pe 's/\s+SSID: |"//g' | awk '$0'
	else
		# older WEXT (Wireless EXTentions) standard
		iwlist wlan0 scan | grep ESSID | perl -pe 's/\s+ESSID:|"//g' | awk '$0'
	fi
}

# get wireless informations
wifi_status()
{
	# check interfaces first
	# and exit if not found
	wifi_check

	# nl80211 (netlink) standard
	IWDEV=`iw dev`
	if [ "$IWDEV" ]; then
		#ip link set wlan0 up
		iw dev wlan0 link
		iw dev wlan0 station dump
	else
		# older WEXT (Wireless EXTentions) standard
		iwconfig wlan0
	fi
}




# ---------------------------------------------------------------------------------------------------------------------
#	ETHERNET & USB TETHERING
#	https://wiki.archlinux.org/index.php/Systemd-networkd
#	require systemd-networkd service: systemctl enable systemd-networkd.service
# ---------------------------------------------------------------------------------------------------------------------


# write ethernet profile (for IOS USB tethering)
# https://wiki.archlinux.org/index.php/IPhone_Tethering
eth_profile()
{

	# DHCP profile
	DHCP=`grep -s DHCP $ETHERNET_PROFILE`
	if [[ "$USBTETHERING_STATIC_ADDRESS" == "" && ! $DHCP ]]; then
		echo "Write dynamic ethernet profile"
		cat << EOF > $ETHERNET_PROFILE
[Match]
Name=eth1

[Network]
DHCP=both
EOF
	fi

	# static address profile
	IP=`grep -E -so "([0-9]{1,3}[\.]){3}[0-9]{1,3}" $ETHERNET_PROFILE`
	if [[ "$USBTETHERING_STATIC_ADDRESS" != "" &&  "$IP" != "$USBTETHERING_STATIC_ADDRESS" ]]; then

		echo "Write static ethernet profile"
		cat << EOF > $ETHERNET_PROFILE
[Match]
Name=eth1

[Network]
Address=$USBTETHERING_STATIC_ADDRESS/24
EOF
	fi

}

# write USB tethering profile (for Androïd USB tethering)
usb_profile()
{

	# DHCP profile
	DHCP=`grep -s DHCP $USBTETHERING_PROFILE`
	if [[ "$USBTETHERING_STATIC_ADDRESS" == "" && ! $DHCP ]]; then
		echo "Write dynamic USB tethering profile"
		cat << EOF > $USBTETHERING_PROFILE
[Match]
Name=usb0

[Network]
DHCP=both
EOF
	fi

	# static address profile
	IP=`grep -E -so "([0-9]{1,3}[\.]){3}[0-9]{1,3}" $USBTETHERING_PROFILE`
	if [[ "$USBTETHERING_STATIC_ADDRESS" != "" &&  "$IP" != "$USBTETHERING_STATIC_ADDRESS" ]]; then

		echo "Write static USB tethering profile"
		cat << EOF > $USBTETHERING_PROFILE
[Match]
Name=usb0

[Network]
Address=$USBTETHERING_STATIC_ADDRESS/24
EOF
	fi

}




# ---------------------------------------------------------------------------------------------------------------------
#	GPRS/3G/4G USB MODEM
# ---------------------------------------------------------------------------------------------------------------------

domodem()
{
	case $@ in
		start )
			modem_start
			;;
		stop )
			modem_stop
			;;
		write )
			modem_profile
			;;
		* )
			usage
			exit 1
			;;
	esac

	# # manage connection
	# echo "${@^} 3G/4G modem connection"
	# case $@ in
	# 	start )
	# 		netctl start `basename $MODEM_PROFILE`
	# 		;;
	# 	stop )
	# 		netctl stop `basename $MODEM_PROFILE`
	# 		;;
	# 	* )
	# 		exit 1
	# 		;;
	# esac
}

# check for modem device
# usb_modeswitch should set up a link to /dev/gsmmodem for /dev/ttyUSB0, /dev/ttyUSB1 … /dev/ttyUSB*
modem_check()
{
	if [[ ! -L /dev/gsmmodem && `ls /dev | grep ttyUSB` == "" ]]; then
		echo "Error: no USB modem found"
	 	exit 1
	fi
}

# start USB modem connection
modem_start()
{
	# check interfaces first
	# and exit if not found
	modem_check
	
	# write profile
	# always write profile to match user manual modifications
	#if [ ! -f $MODEM_PROFILE ]; then
	modem_profile
	#fi
	
	# start
	echo "Start USB modem connection"
	netctl start `basename $MODEM_PROFILE`

}

# stop USB modem connection
modem_stop()
{	
	# check interfaces first
	# and exit if not found
	modem_check

	echo "Stop USB modem connection"
	netctl stop `basename $MODEM_PROFILE`
}

# write USB modem profile
# man netctl.profile for more informations
modem_profile()
{

	# /dev/gsmmodem symlink may not exist on boot
	# select the last interface
	dev=`readlink /dev/gsmmodem`
	if [ ! "$dev" ]; then
		dev=`ls /dev/ttyUSB* | tail -1 | sed 's/\/dev\///'`
	fi

	# write files
	echo "Write USB modem profile"
		cat << EOF > $MODEM_PROFILE
Description="Streambox USB modem connection"
Interface=$dev
Connection=mobile_ppp
AccessPointName=$MODEM_APN
User="$MODEM_USER"
Password="$MODEM_PASSWORD"
PhoneNumber="$MODEM_PHONE"
Pin=$MODEM_PIN
Mode=$MODEM_MODE
MaxFail=0
OptionsFile=/home/locusonus/.modem_ppp_options
EOF
		cat << EOF > /home/locusonus/.modem_ppp_options
noipdefault
persist
EOF

}




# ---------------------------------------------------------------------------------------------------------------------
#	REMOTE ACCESS
# ---------------------------------------------------------------------------------------------------------------------

HTTP_PORT=""
SSH_PORT=""

doremote()
{
	case $@ in
		start )
			remote_start
			;;
		stop )
			remote_stop
			;;
		scan )
			remote_scan
			;;
		check )
			remote_check
			;;
		* )
			usage
			exit 1
			;;
	esac
}

remote_scan()
{
	HTTP_PORT=""
	SSH_PORT=""

	# list ports
	RULES=`upnpc -l`
	HTTP_PORT=`echo "$RULES" | pcregrep -o1 -e "TCP +?([0-9]+).*Streambox web interface"`
	SSH_PORT=`echo "$RULES"  | pcregrep -o1 -e "TCP +?([0-9]+).*Streambox SSH"`

	if [ "$HTTP_PORT" ]; then
		echo "`hostname` HTTP port redirection: $HTTP_PORT"
		echo "`hostname` SSH  port redirection: $SSH_PORT"
	fi
}

remote_stop()
{

	# retrieve previous ports from UPnP config
	remote_scan > /dev/null
	# list local redirection
	RULES=`iptables -t nat -n -L`


	if [ "$HTTP_PORT" ]; then
		echo -n "delete port redirection $HTTP_PORT ... "
		upnpc -d $HTTP_PORT TCP > /dev/null
		HTTP_RULE=`echo "$RULES" | grep "tcp dpt:$HTTP_PORT"`
		if [ "$HTTP_RULE" ]; then iptables -t nat -D PREROUTING -p tcp --dport $HTTP_PORT -j REDIRECT --to-port 80; fi
		echo "done"
	fi
	if [ "$SSH_PORT" ]; then
		echo -n "delete port redirection $SSH_PORT ... "
		upnpc -d $SSH_PORT TCP > /dev/null
		SSH_RULE=`echo "$RULES" | grep "tcp dpt:$SSH_PORT"`
		if [ "$SSH_RULE" ]; then iptables -t nat -D PREROUTING -p tcp --dport $SSH_PORT -j REDIRECT --to-port 22; fi
		echo "done"
	fi

	# clear data in database
	curl --data "usr=${LOGIN}&pass=${PASSWORD}&streambox=0" http://locusonus.org/soundmap/admin/savedata.php
}

remote_start()
{

	# remove previous configuration first
	remote_stop

	# define ports number : ramdomly, in range of 49152 to 65535
	# or using user settings
	if [ ! "$REMOTE_ACCESS_HTTP_PORT" ]; then HTTP_PORT=`echo $[ 49152 + $[ RANDOM % 16383 ]]`;
	else HTTP_PORT=$REMOTE_ACCESS_HTTP_PORT; fi
	if [ ! "$REMOTE_ACCESS_SSH_PORT" ]; then SSH_PORT=`echo $[ 49152 + $[ RANDOM % 16383 ]]`;
	else SSH_PORT=$REMOTE_ACCESS_SSH_PORT; fi

	# add upnp rules on router
	upnpc -e "Streambox web interface (80)" -r $HTTP_PORT TCP > /dev/null
	upnpc -e "Streambox SSH (21)" -r $SSH_PORT TCP > /dev/null

	# check / ouput result
	remote_scan

	# add local filters
	if [ "$HTTP_PORT" ]; then
		iptables -t nat -A PREROUTING -p tcp --dport $HTTP_PORT -j REDIRECT --to-port 80
	fi
	if [ "$SSH_PORT" ]; then
		iptables -t nat -A PREROUTING -p tcp --dport $SSH_PORT -j REDIRECT --to-port 22
	fi

	# save data in database
	if [ "$HTTP_PORT" ]; then
		curl --data "usr=${LOGIN}&pass=${PASSWORD}&streambox={\"port\":{\"http\":$HTTP_PORT,\"ssh\":$SSH_PORT}}" \
		 	 http://locusonus.org/soundmap/admin/savedata.php
	fi

}

# check and refresh remote access configuration
remote_check()
{

	if [ "$REMOTE_ACCESS" == "yes" ]; then
		
		# check UPnP configuration
		remote_scan > /dev/null

		if [ -z "$HTTP_PORT" ]; then RESTART=1; fi
		if [[ "$REMOTE_ACCESS_HTTP_PORT" && "$REMOTE_ACCESS_HTTP_PORT" != "$HTTP_PORT" ]]; then RESTART=1; fi
		if [[ "$REMOTE_ACCESS_SSH_PORT"  && "$REMOTE_ACCESS_SSH_PORT"  != "$SSH_PORT"  ]]; then RESTART=1; fi

		# (re)configure UPnP
		if [ "$RESTART" ]; then
			echo "add router UPnP rules"
			remote_start
			exit 0
		fi


		# check local filters
		RULES=`iptables -t nat -n -L`
		if [ "$HTTP_PORT" ]; then
			HTTP_RULE=`echo "$RULES" | grep "tcp dpt:$HTTP_PORT"`
			if [ -z "$HTTP_RULE" ]; then
				echo "add local HTTP redirection to : $HTTP_PORT"
				iptables -t nat -A PREROUTING -p tcp --dport $HTTP_PORT -j REDIRECT --to-port 80
			fi
		fi
		if [ "$SSH_PORT" ]; then
			SSH_RULE=`echo "$RULES" | grep "tcp dpt:$SSH_PORT"`
			if [ -z "$SSH_RULE" ]; then
				echo "add local SSH redirection to  : $SSH_PORT"
				iptables -t nat -A PREROUTING -p tcp --dport $SSH_PORT -j REDIRECT --to-port 22
			fi
		fi

		# save data in database
		if [[ "$LOGIN" && "$HTTP_PORT" ]]; then
			curl --data "usr=${LOGIN}&pass=${PASSWORD}&streambox={\"port\":{\"http\":$HTTP_PORT,\"ssh\":$SSH_PORT}}" \
			 	 http://locusonus.org/soundmap/admin/savedata.php
		fi
	fi

}




# ---------------------------------------------------------------------------------------------------------------------
#	MAIN FUNCTIONS
# ---------------------------------------------------------------------------------------------------------------------

auto_connect()
{
	ec=0

	if [[ "$WIFI_ENABLE" == "yes" && `systemctl is-active netctl@wlan` != 'active' ]]; then
		echo "Opening wireless connection ..."
		err=`wifi_start 2>&1`
		rc=$?; if [[ $rc != 0 ]]; then echo -e "\033[1;31m$err\033[0m" 1>&2; ec=1; fi
	fi

	if [[ "$MODEM_ENABLE" == "yes" && `systemctl is-active netctl@modem` != 'active' ]]; then
		echo "Opening USB modem connection ..."
		err=`modem_start 2>&1`
		rc=$?; if [[ $rc != 0 ]]; then echo -e "\033[1;31m$err\033[0m" 1>&2; ec=1; fi
	fi

	# wait a minute (for network to be configured)
	# and check/update remote access configuration
	sleep 60 && echo "Checking remote access configuration ..." && remote_check

	exit $ec

}

configure()
{
	case $@ in
		wifi )
			wifi_profile
			;;
		eth )
			eth_profile
			;;
		usb )
			usb_profile
			;;
		modem )
			modem_profile
			;;
		* )
			usage
			exit 1
			;;
	esac
}

usage()
{
	echo "$0 {COMMAND} ..."
	echo ""
	echo "Streambox network manager"
	echo 
	echo "  -h --help                       show this help"
	echo "  --hostname                      set hostname and reboot"
	echo "  --autoconnect                   connect to all network enabled"
	echo "  --profile usb|eth|wifi|modem    configure network link profile"
	echo "  --wifi start|stop|scan|write    manage wireless connection, write profile or list available access point"
	echo "  --modem start|stop|write        manage GPRS/3G/4G USB modem connection"
	echo "  --remote start|stop|scan|check  add or delete ports redirection"
	echo "                                  i.e. enable/disable remote access"
}

case $1 in
	--hostname )
		sethostname
		;;
	--wifi )
		dowifi $2
		;;
	--profile )
		configure $2
		;;
	--modem )
		domodem $2
		;;
	--remote )
		doremote $2
		;;
	--autoconnect )
		auto_connect
		;;
	-h|* )
		usage
		exit 1
		;;
esac
exit $?

