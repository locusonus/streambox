
/**
 * File: statusled.cpp
 * Version: 0.1
 * Authors: Gégroire Lauvin <http://gregorth.net> and Stéphane Cousot <http://www.ubaa.net/>
 * License: statusled is released under GNU General Public License, version 3 or later
 *
 * Interact with the LED status display (web and on Air).
 * for more informations , see <http://locusonus.org/streambox/statusled/>
 *
 * -------------------------------------------------------------------------------------------------------
 *
 * 	- to install dependencies:
 * 	  pacman -S wiringpi gcc
 * 	- to compile this script:
 * 	  c++ -std=gnu++11 -o /home/locusonus/bin/statusled /home/locusonus/dev/statusled.cpp -lcurl -lwiringPi
 *
 * --------------------------------------------------------------------------------------------------------
 *
 *	This file is part of the Locus Sonus streambox project.
 *	for more informations, see <http://locusonus.org/streambox/>
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#define TIMEOUT 40
#define CHECK   2
#define PIN_AIR	9
#define PIN_WEB 8
#define PIN_OFF 0
#define PIN_LIVE 2
#define PIN_DETECT 7


#include <wiringPi.h>
#include <curl/curl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>
#include <regex>
#include <signal.h>



using namespace std;


volatile sig_atomic_t run = 0;

int checkNetworkIsOnline()
{
	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();
	if ( curl ) {
		curl_easy_setopt( curl, CURLOPT_URL, "www.google.com" );
		curl_easy_setopt( curl, CURLOPT_CONNECTTIMEOUT, TIMEOUT );
		curl_easy_setopt( curl, CURLOPT_NOBODY, 1 );
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
	} 

	return res==CURLE_OK;

}

const char * getStreamUrl()
{
	smatch match;
	string line;
	string url;

	ifstream conf("/boot/darkice.cfg");
	regex pattern("^url.*=\\s+(.*)");

	if ( conf.is_open() ) {
		while( !conf.eof() ) {
			getline( conf, line );
			regex_search( line, match, pattern );
			if ( match.size() ) url = match[1];
		}
		if ( url.empty() ) url = "url not found !";
	}
	else url = "configuration file not found !";

	conf.close();
	return url.c_str();
}

int checkStreamIsActive()
{
	CURL *curl;
	CURLcode res;
	
	long code = 0;
	char url[1024];

	curl = curl_easy_init();
	if ( curl ) {
		sprintf( url, "http://locusonus.org/soundmap/checkurl/?q=%s", getStreamUrl() );
		curl_easy_setopt( curl, CURLOPT_URL, url );
		curl_easy_setopt( curl, CURLOPT_CONNECTTIMEOUT, TIMEOUT );
		curl_easy_setopt( curl, CURLOPT_NOBODY, 1 );
		res = curl_easy_perform(curl);
		curl_easy_getinfo( curl, CURLINFO_RESPONSE_CODE, &code );
		curl_easy_cleanup(curl);
		return code==200;
	}

	return 0;
}

static void sighandler( int signum ) { run=0; }


int main(int argc, char *argv[])
{
	
	int opt, res;
	while( (opt=getopt(argc, argv, "nsh"))!=-1 ) switch(opt)
	{
		case 'n':
			res = checkNetworkIsOnline();
			printf( "Network is online ... %s\n", res ? "yes" : "no" );
			return !res;
		case 's':
			res = checkStreamIsActive();
			printf( "Stream url ......... %s\n", getStreamUrl() );
			printf( "Stream is active ... %s\n", res ? "yes" : "no" );
			return !res;
		case 'h':
		case '?':
			printf( "usage:\n" );
			printf( "-n check if network is online\n" );
			printf( "-s check if the stream is active\n" );
			return 0;
	}

	// handle kill signal
	struct sigaction sa;
	sa.sa_handler = sighandler;
	sigemptyset(&sa.sa_mask);
	sigaction( SIGINT, &sa, NULL );



	wiringPiSetup () ;
	
  	pinMode (PIN_WEB, OUTPUT) ; //WEB LED
  	pinMode (PIN_AIR, OUTPUT) ; //AIR LED
  	pinMode (PIN_LIVE, OUTPUT) ; //LIVE pin
  	pinMode (PIN_OFF, INPUT) ; //off button
  	pinMode (PIN_DETECT, INPUT) ; //card is active or not
  	pullUpDnControl (PIN_OFF,PUD_DOWN) ;
  	pullUpDnControl (PIN_DETECT,PUD_DOWN) ;
  	int count=CHECK;
  	int airStatus=0;


  	// detect the card
  	run = digitalRead(PIN_DETECT);
  	if ( !run ) {
  		fprintf( stderr, "LED status display not found !\n" );
  		return 1;
  	}
    
    
	while( run ) {

		count++;
		if(count>=CHECK){
			count=0;
			digitalWrite(PIN_WEB,checkNetworkIsOnline()); //light WEB LED according to network status
			airStatus=checkStreamIsActive(); //light AIR LED according to stream status
		}
		digitalWrite(PIN_AIR,airStatus);
		digitalWrite(PIN_LIVE,1);
		delay(500);
		digitalWrite(PIN_AIR,0);
		digitalWrite(PIN_LIVE,0);
		delay(500);
	    

	    //power off on button event
	 	if(digitalRead(PIN_OFF)){
	 		delay(500);//ignore acidental use
	 		if(digitalRead(PIN_OFF)){
	 		 	//and blink for style
	 		 	digitalWrite(PIN_WEB,0);
	 		 	digitalWrite(PIN_AIR,0);
	 		 	delay(250);
	 		 	digitalWrite(PIN_WEB,1);
	 		 	digitalWrite(PIN_AIR,1);
	 		 	delay(250);
	 		 	digitalWrite(PIN_WEB,0);
	 		 	digitalWrite(PIN_AIR,0);
	 		 	system("poweroff");
	 		 	run = 0;
	 		 }
	 	}
	}

	// turn off leds
	digitalWrite(PIN_WEB,0);
	digitalWrite(PIN_AIR,0);
	
	return 0;
}
