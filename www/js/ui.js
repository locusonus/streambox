/**
 *  (./) ui.js
 *  (by) Stéphane Cousot <http://www.ubaa.net/>
 *  (cc) some right reserved
 *
 *  -------------------------------------------------------------
 *
 *  User interaction, request/response dispatcher, frontend process.
 *  version 3.1
 *  
 *  -------------------------------------------------------------
 *
 *  This file is part of the Locus Sonus streambox project.
 *  for more informations, see <http://locusonus.org/streambox/>
 *
 *  This file is released under a Creative Commons Licence BY
 *  http://creativecommons.org/licenses/by/4.0/
 */


var HOST = window.location.hostname;
var CGI  = "";
var CFG  = "/boot/darkice.cfg";


const LOG_DELAY = 60*1000;
const AJAX_TIMEOUT_DELAY = 20*1000;
const AJAX_PING_FREQ = 7*1000;
const FADEOUT_OK_DELAY = 1600;
const FADEOUT_ERR_DELAY = 6000;



$(document).ready(function()
{

	// update all audio input interaction
	audioinit();

	// update CGI reference
	CGI  = $("#config").attr("action");

	// submit button
	$("#config").find("input[type='submit']").hide();
	var $submit = $("<a class='submit'>save</a>").insertBefore( $("#config").parent() ).click(function(){
		$("#config").submit();
	}).hide();

	// handle submit
	$("#config").submit(function( event )
	{
		event.preventDefault();

		var $form = $(this);
		var host  = $form.find( "input[name='HOSTNAME']" ).val();
		var usbip = $("input[name='USBTETHERING_STATIC_ADDRESS']").val();

		// validate
		if ( !$.trim(host) ) {
			dialog("hostname could not be empty");
			return;
		}
		if ( !host.match(/^[a-zA-Z]+[\w\-]+\w+$/) ) {
			dialog( "INVALID HOSTNAME", {
				content: "Please match the requested format.\nRead the documentation for more information."
			});
			return;
		}
		if ( !$('#usb_dhcp').is(':checked') && !$('#usb_ip').val().match(/^(\d{1,3}\.){3}\d{1,3}$/) ) {
			dialog( "INVALID USB TETHERING ADDRESS", {
				content: "Please match the requested format (e.g. 192.168.2.230)"
			});
			return;
		}


		$("input[name='USBTETHERING_STATIC_ADDRESS']").val( $('#usb_dhcp').is(':checked')?'':$('#usb_ip').val() );


		// send the data
		$(".submit").hide();
		$("#config_resp").removeClass("error").text("saving…").show();
		$.post( CGI, $form.serialize(), function(data)
		{
			if ( data.code==200 ) {
				$("#config_resp").text(data.message).fadeOut( FADEOUT_OK_DELAY );

				var ask = host+".local"!=HOST;
				HOST = host + ".local";
				$("span.hostname").text( host );
				$("span.host").text( HOST );
				if ( ask ) dialog(
					"THE HOSTNAME HAS CHANGED", {
					content: "Your new hostname will be available the next boot.\nDo you want to reboot now?",
					action: reboot
				});
				if ( $("input[name='USBTETHERING_STATIC_ADDRESS']").val()!=usbip ) dialog(
					"THE USB TETHERING CONFIGURATION HAS CHANGED", {
					content: "Your USB Tethering configuration will be available the next boot.\nDo you want to reboot now?",
					action: reboot
				});
			}
			else $("#config_resp").addClass("error").text("error: "+data.message).show().fadeOut(FADEOUT_ERR_DELAY);

		}, "json");

		return true;

	});

	// load current streambox configuration
	$.ajax({
		url: CGI,
		data: { do:"load" },
		type: "POST",
		dataType: "json",
		timeout: AJAX_TIMEOUT_DELAY,
	})
	.done(function(data)
	{
		if ( data.code ) {
			$("#config_resp").addClass("error").text("error: "+data.message).show();
		}

		var $form = $("#config");
		for( var p in data ) {
			switch(p) {

				// checkbox
				case 'VIRTUAL_STEREO' :
				case 'WIFI_ENABLE' :
				case 'WIFI_SSID_IS_HIDDEN' :
				case 'WIFI_KEY_IS_HEXADECIMAL' :
				case 'USBTETHERING_USE_DHCP' :
				case 'MODEM_ENABLE' :
				case 'STATUSLED' :
				case 'REMOTE_ACCESS' :
				case 'CIRRUS_HEADPHONE' :
				case 'CIRRUS_LINE_OUT' :
				case 'CIRRUS_SPEAKER' :
				case 'CIRRUS_SPDIF_OUT' :
					$("input[name='"+p+"']").prop('checked', data[p]=='yes');
					break;

				case 'WIFI_SECURITY' :
					$("input[name='WIFI_SECURITY'][value='"+data.WIFI_SECURITY+"']").prop('checked', true);
					break;

				case 'MODEM_MODE' :
					$("input[name='MODEM_MODE'][value='"+data.MODEM_MODE+"']").prop('checked', true);
					break;

				case 'CIRRUS_INPUT' :
					$("input[name='CIRRUS_INPUT'][value='"+data.CIRRUS_INPUT+"']").prop('checked', true).trigger('click');
					break;

				case 'AUDIO_CHANNEL' :
					$("input[name='AUDIO_CHANNEL'][value='"+data.AUDIO_CHANNEL+"']").prop('checked', true);
					break;

				case 'AUDIO_QUALITY' :
					$("input[name='AUDIO_QUALITY']").val( data[p]*10 );
					$("#quality").text( data[p] );
					break;

				case 'AUDIO_SAMPLERATE' :
					$("select[name='AUDIO_SAMPLERATE']").val( data[p] );
					break;

				case 'SOUNDCARD_LIST' :
					var i = 0;
					$.each( data[p], function(key,value) {
						$("select[name='SOUNDCARD']").append($("<option></option>")
							.attr("value",key)
							.text(value.replace("RPi-Cirrus"," Cirrus Logic/Wolfson audio card"))
							.prop("selected",parseInt(data.SOUNDCARD)==key?"selected":""));
						i++;
						if ( value=="RPi-Cirrus")  $(".cirrus").toggle();
						if ( value=="Blue Icicle") $(".blueicicle").toggle();
					});
					$("select[name='SOUNDCARD']").prop('disabled', i<2);
					$("input[name='VIRTUAL_STEREO']").prop('disabled', i<2);
					break;

				default:
					$form.find( "input[name='"+p+"']" ).val( data[p] );
					break;
			}
		}

		$form.find(":input").keyup(function(){
			$submit.show();
		}).change(function(){
			$submit.show();
		});


		$('input.gain').trigger('input');

		if ( $("select[name='SOUNDCARD'] option").length==1 )
			 $("input[name='VIRTUAL_STEREO']" ).prop('checked', false );

		if ( !data.USBTETHERING_STATIC_ADDRESS ) {
			$("#usb_bloc").hide();
			$("#usb_dhcp").prop('checked', true);
		}
		$("#usb_ip").val( data.USBTETHERING_STATIC_ADDRESS );
		$("#usb_dhcp").change(function(){ $("#usb_bloc").toggle(); });


		if ( data.HOSTNAME ) HOST = data.HOSTNAME + ".local";
		$("span.host").text( HOST );

		if ( !data.LOGIN ) {
			$form.parent().toggle();
			$form.parent().siblings("h2").addClass("open");
		}
		$("#config_resp").fadeOut( data.code ? FADEOUT_ERR_DELAY : FADEOUT_OK_DELAY );

	})
	.always( function()
	{
		// retrieve stream/network status
		// and darkice config
		log();
		load(CFG,"stream_cfg");

		// get system information
		//exec( "uname -snrmo", "system_log" );
		$.post( CGI, {do:"sysinfo"}, function(content){ $('#system_log').text(content); }, "text");
	});

	// hide element
	$("#audio_resp").hide();

	// add toggle panel
	$("h2").click(function(e)
	{
		$(this).siblings(".subpanel").toggle();
		$(this).toggleClass("open");
	});

});



// stream status
var tlog = null;
function log(force)
{
	if ( force ) $("#status").text("refresh status…").removeClass("error");

	$.ajax({
		type: "POST",
		url: CGI,
		data: { do:"log" },
		timeout: AJAX_TIMEOUT_DELAY,
		dataType: "json"
	})
	.done(function(data)
	{
		$("#dialog").fadeOut();
		$("#status").text(data.status).toggleClass("error",!data.active);
		$("#log").text(data.log);
		$("#cpu").text(data.cpu+' %');

		if ( !force ) netctl();
	})
	.fail( function()
	{
		dialog(
			'<a href="./">Your stream box</a> is not responding',{
			content : 'Please check your network connection, '+
			'cables, devices, (etc.), and wait a moment '+
			'before rebooting.',
			action: false
		});
	})
	.always( function()
	{
		if ( !tlog ) tlog = setInterval( log, LOG_DELAY );
	});
}

// stream control
function streamctl(what)
{
	if ( tlog ) {
		clearInterval(tlog);
		tlog = null;
	}
	$("#status").removeClass("error").text(what+" wait…");
	$.post( CGI, {do:"stream",ctl:what}, function()
	{
		log(true);
		load(CFG,"stream_cfg");
	});
}

// network control
function netctl( msg, link, what )
{
	if ( !msg ) msg = 'refresh status…';
	$("#net_resp").text(msg).removeClass("error").show();
	$.post( CGI, {do:"net",lnk:link,ctl:what}, function(data)
	{
		$("#net_resp").fadeOut( FADEOUT_OK_DELAY );

		if ( link=="wifi" && data.wifi.error ) 
			$("#net_resp").text(data.wifi.error).toggleClass("error",!data.wifi.active).stop().show();
		if ( link=="modem" && data.modem.error ) 
			$("#net_resp").text(data.modem.error).toggleClass("error",!data.modem.active).stop().show();
		if ( link=="usb" && data.usb.error ) 
			$("#net_resp").text(data.usb.error).toggleClass("error",!data.usb.active).stop().show();
		
		$("#wan_ip").text(data.net.wan);
		$("#lan_ip").text(data.net.lan);
		$("#eth_stat").text(data.eth.status);
		$("#usb_stat").text(data.usb.status);

		if ( data.net.port ) $("#remoteaccess_stat").html(
			"external interface address: http://"+data.net.wan+":"+data.net.port.http+"<br>"+
			"external ssh access: ssh -p "+data.net.port.ssh+" locusonus@"+data.net.wan
		)
		else $("#remoteaccess_stat").text("");

		if ( data.wifi.log ) {
			$("#wifi_stat").text(data.wifi.status);
			$("#wifi_log").text(data.wifi.log);
		}
		else if ( !data.wifi.error && data.wifi.result ) {
			dump(data.wifi.result);
		}

		if ( data.modem.log ) {
			$("#modem_stat").text(data.modem.status);
			$("#modem_log").text(data.modem.log);
		}
		else if ( !data.modem.error && data.modem.result ) {
			dump(data.modem.result);
		}
		
	}, "json");
}


// load file
function load( file, where )
{
	$.post( CGI, {do:"file","file":file}, function(content){
		$('#'+where).text(content);
	}, "text");
}

// execute command
function exec( cmd, where )
{
	$.post( CGI, {"do":"exec","cmd":cmd}, function(content){
		if ( !where ) dump(content);
		else $('#'+where).text(content);
	}, "text");
}

// shutdown
function shutdown()
{
	dialog( "shutdown…", { class:'wait', action:false } );
	
	$.ajax({
		type: "POST",
		url: CGI,
		data: { do:"shutdown" },
		timeout: AJAX_TIMEOUT_DELAY,
		dataType: "text"
	})
	.done(function(msg)
	{
		$("#dialog").fadeOut();
		$("#system_resp").text(msg).show();
	})
	.fail( function()
	{
		dialog( "<a href='http://"+HOST+"/'>"+HOST+"</a> is OFF", { class:'halt', action:false } );
		if ( tlog ) {
			clearInterval(tlog);
			tlog = null;
		}
	});
}

// reboot
function reboot()
{

	dialog( "<a href='http://"+HOST+"/'>rebooting</a> … wait", { class:'wait', action:false } );
	if ( tlog ) {
		clearInterval(tlog);
		tlog = null;
	}

	var xhr = null;
	var timer = setInterval( function(){
		if ( xhr ) xhr.abort();
		xhr = $.ajax({
			url: "http://"+HOST+"/"+$("#config").attr("action"),
			type: "HEAD",
			crossDomain: true
		})
		.done( function() { window.location = "http://"+HOST+"/"; });
	},
	AJAX_PING_FREQ);

	$.post( CGI, {do:"reboot"}, function(msg)
	{
		if ( timer ) {
			clearInterval(timer);
			timer = null;
		}
		$("#dialog").fadeOut();
		$("#system_resp").text(msg).show();

	}, "text");
}

// open message in dialog window
// optionnal args : { class:*, content:*, action:* }
function dialog( text, args )
{
	if ( !text ) return;

	if ( typeof(args)!='object' ) args = {};
	if ( !args.class ) args.class = "warning";

	var content = "<p class='title'>" + text + "</p>";
	if ( args.content ) content += "<p class='message'>"+ ( args.content.replace(/\n/g,"<br>") ) +"</p>";

	// alert
	if ( typeof(args.action)=='undefined' ) {
		content += "<p class='action'>";
		content += "<input type='button' value='Ok' onclick=\"$('#dialog').fadeOut();\">";
		content += "</p>";
	}

	// confirm
	if ( typeof(args.action)=='function' ) {
		content += "<p class='action'>";
		content += "<input type='button' value='Cancel' onclick=\"$('#dialog').fadeOut();\">";
		content += "<input type='button' value='Ok'>";
		content += "</p>";
	}

	$("#dialog").html( "<div class='"+args.class +"'>"+content+"</div>" ).stop().show();
	$("#dialog").find("input[type='button']").each(function(i)
	{
		if ( !i ) $(this).focus();
		else $(this).click(function(){$('#dialog').fadeOut();}).click(args.action);
		$('#dialog > div').css("padding-bottom", 0);
	});
}

// open message in console
function dump( msg )
{
	if ( msg ) {
		$("#console").show().click( function(e)
		{
			if ( e.target==this ) $(this).fadeOut();
		});

		var pre = $("#console").find("pre");
		pre.text(msg);
		if ( pre.height()>$(window).height()-120 ) pre.css( "height", $(window).height()-120 );
		else pre.css( "height", "auto" );
		pre.hide().slideDown();
	}
}

// toggle element
function toggle( elem, target )
{
	var t = $( elem ).text().replace(/View|Hide/,'');

	$( '#'+target ).toggle();
	$( elem ).text( $('#'+target).is(":hidden") ? "View"+t : "Hide"+t );
}

// open stream's admin page
function streamadmin(elem)
{
	var $conf = $("#config");
	$('<form action="'+$(elem).attr("href")+'login/" method="POST" target="new">')
	.append($('<input type="hidden" name="AUTH_USER" value="' + $conf.find( "input[name='LOGIN']" ).val() + '">'))
	.append($('<input type="hidden" name="AUTH_PW" value="' + $conf.find( "input[name='PASSWORD']" ).val() + '">'))
	.append($('<input type="hidden" name="redirect" value="user">'))
	.appendTo($(document.body)) //it has to be added somewhere into the <body>
	.submit();
}


// update audio input
function audioinit()
{

	// handle selected input default gain
	$( 'input[name=CIRRUS_INPUT]' ).click(function(){

		var pga = dig = 0;
		var hide = [];
		var mono = false;

		switch( $(this).val() ) {
			case "dmic"	   : dig = -6; hide = ['pga']; break;
			case "mic"	   : pga = 20; mono = true; break;
			case "linein"  : pga = 8; break;
			case "micbias" : pga = 20; break;
			case "spdifin" : hide = ['pga','dig']; break;
		}

		$('input[name=CIRRUS_INL_VOL]').val(0).trigger('input');
		$('input[name=CIRRUS_INR_VOL]').val(0).trigger('input');
		$('input[name=CIRRUS_INL_PGA]').val( pga ).trigger('input');
		$('input[name=CIRRUS_INR_PGA]').val( pga ).trigger('input');
		$('input[name=CIRRUS_INL_DIG]').val( dig ).trigger('input');
		$('input[name=CIRRUS_INR_DIG]').val( dig ).trigger('input');

		$('#cirrus span.inl').toggleClass( 'hide', mono );
		if ( mono ) $('#cirrus span.inr').toggleClass( 'hide', false );
		else {
			$('#cirrus span.pga').toggleClass( 'hide', $.inArray('pga',hide)!=-1 );
			$('#cirrus span.dig').toggleClass( 'hide', $.inArray('dig',hide)!=-1 );
		}

	});

	// add slider output value
	// value to DB  = ( value-(Math.abs(min)/step) )*step
	// DB to value  = ( value+Math.abs(min) ) / step
	$('input.gain').each(function(){
		$(this).parent().attr( 'class', $(this).attr('class') );
		$(this).after( '<span id="'+$(this).attr('name')+'" class="gain"></span>' );
		$(this).on('input',function(){
			var val = parseFloat( $(this).val() );
			var sign = val==0 ? ' ' : (val<0 ? '-' : '+');
			$('#'+$(this).attr('name')).text( sign+' '+Math.abs($(this).val()).toFixed(2)+' dB' );
		});
	});
}

// test audio card
function audiotest(what)
{
	$.ajax({
		url: CGI,
		data: {"do":"exec","cmd":"sudo /home/locusonus/bin/audio.sh --test "+what},//,"quiet":true},
		type: "POST",
		dataType: "text",
		timeout: 3000,
	})
	.done(function(msg)
	{
		if ( msg ) dump(msg);
		$("#audio_resp").removeClass("warning").fadeOut( FADEOUT_OK_DELAY );
	});

	if ( what=="stop"  ) $("#audio_resp").removeClass("warning").fadeOut( FADEOUT_OK_DELAY );
	else $("#audio_resp").html("running test <span class=\"running\"></span>").addClass("warning").show();
}





