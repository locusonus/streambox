<?php

	/**
	 *	(./) dispatcher.php
	 *	(by) Stéphane Cousot <http://www.ubaa.net/>
 	 *	(cc) some right reserved
	 *
	 *	-------------------------------------------------------------
	 *
	 *	User request's dispatcher, backend process.
	 *	version 3.1
	 *
	 *	-------------------------------------------------------------
	 *
	 *	This file is part of the Locus Sonus streambox project.
	 *	for more informations, see <http://locusonus.org/streambox/>
	 *
	 *	This file is released under a Creative Commons Licence BY
	 *	http://creativecommons.org/licenses/by/4.0/
	 */



	// config files
	define( "STREAMBOX_eCONFIG", "/home/locusonus/conf/streambox" ); 
	define( "STREAMBOX_CONFIG", "/boot/streambox.txt" );
	define( "DARKICE_CONFIG", "/boot/darkice.cfg" );

	// systemd servive(s)
	define( "SERVICE_STREAM", "streambox-darkice" );
	define( "SERVICE_STATUSLED", "streambox-statusled" );
	define( "SERVICE_WLAN", "netctl@wlan" );
	define( "SERVICE_MODEM", "netctl@modem" );


	# handle reboot 
	if ( $_SERVER['REQUEST_METHOD']==='HEAD' ) die;

	# accept only POST from host referer
	# or respond with a 403 status code
	if ( !$_POST || !preg_match("/^http:\/\/${_SERVER['HTTP_HOST']}\//", $_SERVER["HTTP_REFERER"]) )
	{
		header("${_SERVER['SERVER_PROTOCOL']} 403 Forbidden");
		die;
	}


	# dispatch
	header('Content-Type: text/plain; charset=utf-8');
	switch( $_POST['do'] )
	{
		case 'load'		: do_loadconf(); break;
		case 'file'		: do_loadfile($_POST['file']); break;
		case 'save'		: do_saveconf(); break;
		case 'log'		: do_log(); break;
		case 'stream'	: do_stream($_POST['ctl']); break;
		case 'net'		: do_net($_POST['lnk'],$_POST['ctl']); break;
		case 'exec'		: do_exec($_POST['cmd'],$_POST['quiet']); break;
		case 'sysinfo'	: do_sysinfo(); break;
		case 'reboot'	: do_reboot(); break;
		case 'shutdown' : do_shutdown(); break;
		
		default: break;
	}


	# -----------------------------------------------------------------------------------------------------------------
	# 	CONFIG
	# -----------------------------------------------------------------------------------------------------------------

	function do_loadconf()
	{

		if ( !file_exists(STREAMBOX_CONFIG) ) die(json_encode(["code"=>404,"message"=>"No such file or directory"]));
		if ( !is_readable(STREAMBOX_CONFIG) ) die(json_encode(["code"=>403,"message"=>"Permission denied"]));

		$data = @file_get_contents(STREAMBOX_CONFIG) or die(json_encode(["code"=>204,"message"=>"Empty settings"]));

		preg_match_all( '/(^|#)(\w.*)="(.*)"/m', $data, $matched );
		$vars = array_combine( $matched[2], $matched[3] );
		$vars["HOSTNAME"] = $vars["HOSTNAME"] ?: "streambox";

		preg_match_all('/card (\d+).*\[(.*)\],/', shell_exec('sudo arecord -l'), $cards );
		$vars["SOUNDCARD_LIST"] = @array_combine( $cards[1], $cards[2] );

		print json_encode($vars);
	}


	function do_saveconf()
	{
		$temp = !is_writable( STREAMBOX_CONFIG );
		$file = !$temp ? STREAMBOX_CONFIG : tempnam(sys_get_temp_dir(),'streambox-'); 

		if ( !is_readable(STREAMBOX_eCONFIG) ) die(json_encode(["code"=>403,"message"=>"Permission denied"]));
		$data = @file_get_contents(STREAMBOX_eCONFIG);

		if ( !$_POST['VIRTUAL_STEREO'] ) $_POST['VIRTUAL_STEREO'] = "no";
		if ( !$_POST['WIFI_ENABLE'] ) $_POST['WIFI_ENABLE'] = "no";
		if ( !$_POST['WIFI_KEY_ISHEX'] ) $_POST['WIFI_KEY_ISHEX'] = "no";
		if ( !$_POST['WIFI_SSID_IS_HIDDEN'] ) $_POST['WIFI_SSID_IS_HIDDEN'] = "no";
		if ( !$_POST['TETHERING_ENABLE'] ) $_POST['TETHERING_ENABLE'] = "no";
		if ( !$_POST['REMOTE_ACCESS'] ) $_POST['REMOTE_ACCESS'] = "no";
		if ( !$_POST['MODEM_ENABLE'] ) $_POST['MODEM_ENABLE'] = "no";
		if ( !$_POST['STATUSLED'] ) $_POST['STATUSLED'] = "no";
		if ( !$_POST['CIRRUS_HEADPHONE'] ) $_POST['CIRRUS_HEADPHONE'] = "no";
		if ( !$_POST['CIRRUS_LINE_OUT'] ) $_POST['CIRRUS_LINE_OUT'] = "no";
		if ( !$_POST['CIRRUS_SPEAKER'] ) $_POST['CIRRUS_SPEAKER'] = "no";
		if ( !$_POST['CIRRUS_SPDIF_OUT'] ) $_POST['CIRRUS_SPDIF_OUT'] = "no";

		$_POST['AUDIO_QUALITY'] = $_POST['AUDIO_QUALITY']/10; 

		foreach( $_POST as $key=>$value ) {
			$data = preg_replace("/(^|#)$key(\s+)?=.*$/m", "$key=\"$value\"", $data );
		}

		do_service( SERVICE_STATUSLED, $_POST['STATUSLED']=="yes" );
		do_remote( $_POST );

		$handle = fopen( $file, "w" );
		if ( $handle ) {
			fwrite( $handle, $data );
			fclose( $handle );
		}
		
		if ( $temp ) system( "sudo /usr/bin/cp $file ".STREAMBOX_CONFIG );
		system( "sudo /home/locusonus/bin/audio.sh --setup" );

		print json_encode(["code"=>200,"message"=>"saved"]);

	}

	function parse_config()
	{
		$config = @file_get_contents(STREAMBOX_CONFIG);
		$config = preg_replace( "/#.*/", "", $config );
		
		return parse_ini_string($config);
	}




	# -----------------------------------------------------------------------------------------------------------------
	#	STREAM
	# -----------------------------------------------------------------------------------------------------------------

	function do_log()
	{
		$log = shell_exec("sudo systemctl status -n 20 ".SERVICE_STREAM);
		preg_match("/.*Active:(.*)/i", $log, $matched );

		// check for error
		if ( $log ) {
			if ( preg_match("/can't open connector/", $log) )
				$matched[1] = "audio encoder error: can't open audio device";
			if ( preg_match("/No such dev/", $log) )
				$matched[1] = "audio encoder stop: no such device or error";
			if ( preg_match("/(auto-restart)/", $log) )
				$matched[1] = "audio encoder restart: wait…";
			if ( preg_match("/curl: \(\d+\) (.*)/", $log, $error) )
				$matched[1] = $error[1];
		}
		else $matched[1] = $log = "Service not found: ".SERVICE_STREAM;


		// check for misconfiguration
		if ( preg_match("/^error(\s+)?=(\s+)?(.*)/m", @file_get_contents(DARKICE_CONFIG), $error) ) 
			$matched[1] = $error[3];


		// get system load average
		$cpu = shell_exec( "ps -C darkice -o %cpu | sed -n 2p" );

		print json_encode([
			"status" => $matched[1],
			"active" => stripos($matched[1],"running")!==false,
			"log" => $log,
			"cpu" => $cpu ?: 0
		]);
	}

	function do_stream( $what )
	{
		system("sudo systemctl $what ".SERVICE_STREAM);
	}




	# -----------------------------------------------------------------------------------------------------------------
	#	NETWORK
	# -----------------------------------------------------------------------------------------------------------------

	function do_net( $link, $what )
	{
		print json_encode([
			"eth"   => link_eth(),
			"usb"   => link_usb(),
			"wifi"  => link_wlan( $link=="wifi" ? $what : false ),
			"modem" => link_modem( $link=="modem" ? $what : false ),
			"net"   =>
			[
				"lan"  => shell_exec("hostname -i"),
				"wan"  => @file_get_contents("http://locusonus.org/streambox/ip"),
				"port" => remote_port()
			]
		]);
	}

	function netstatus( $link, $list=false )
	{
		$status  = shell_exec("networkctl status $link");
		$traffic = nettraffic( $link ); 
		
		preg_match("/^\s+state: (.*)/im", $status, $state );
		preg_match("/^\s+address: (.*)/im", $status, $address );
		preg_match("/\sdns: (.*)\s+(.*)/im", $status, $dns );
		preg_match("/^\s+model: (.*)/im", $status, $model );

		$dns = $dns[1][0] ? $dns[2] ? $dns[1]."/".$dns[2] : $dns[1] : "";

		if ( !$list ) return sprintf(
			"state: %s\naddress: %s%s%s",
			$state[1], $address[1], $dns ? "\nDNS: ".$dns:"", $traffic?"\n$traffic":""
		);

		return array(
			"model" => $model[1],
			"state" => $state[1],
			"address" => $address[1],
			"DNS" => $dns,
			$traffic
		);
	}

	function nettraffic( $link )
	{
		$ifconfig = shell_exec("ifconfig $link");

		preg_match("/^\s+RX packets.*\((.*)\)/im", $ifconfig, $rx );
		preg_match("/^\s+TX packets.*\((.*)\)/im", $ifconfig, $tx );

		return $rx||$tx ? "totals upload: ${tx[1]}\ntotals download: ${rx[1]}" : "";

	}

	function link_eth()
	{
		return array(
			"status" => netstatus("eth0")
		);
	}

	function link_wlan( $ctl )
	{

		if ( $ctl ) {
			$log = shell_exec("sudo /home/locusonus/bin/network.sh --wifi $ctl 2>&1");
			if ( preg_match("/error: (.*)/i", $log, $matched) ) $error = $matched[1];
			if ( $ctl=="scan" || $ctl=="status" ) {
				return array(
					"error"  => $error,
					"result" => $log
				);
			}
		}


		$log = shell_exec("sudo systemctl status -n 20 ".SERVICE_WLAN);
		preg_match("/.*Active: (\w+) /i", $log, $matched );

		if ( preg_match("/failed/i", $log) )
			$error = "Wi-Fi error: view log for more details";
		if ( preg_match("/input\/output error/i", $log) )
			$error = "Wi-Fi error: input/output error";
		if ( preg_match("/interface 'wlan0' does not exist/i", $log) )
			$error = "no Wireless interface found";

		return array(
			"status" => netstatus("wlan0"),
			"error"  => $error,
			"active" => $matched[1]=="active",
			"log" => $log
		);
	}

	function link_usb()
	{

		$eth = netstatus("eth1", true);
		$usb = netstatus("usb0", true);

		if ( $eth['state'] )
			$status = implode("\n", array_map(function($v,$k){ return $k?"$k: $v":$v; }, $eth, array_keys($eth)));

		if ( $usb['state'] ) {
			if ( $eth['state'] ) $status .= "\n\n";
			$status .= implode("\n", array_map(function($v,$k){ return $k?"$k: $v":$v; }, $usb, array_keys($usb)));
		}

		return array(
			"status" => $status ?: "state:\naddress:"
		);
	}

	function link_modem( $ctl )
	{

		if ( $ctl ) {
			$log = shell_exec("sudo /home/locusonus/bin/network.sh --modem $ctl 2>&1");
			if ( preg_match("/error: (.*)/i", $log, $matched) ) $error = $matched[1];
			/*if ( $ctl=="status" ) {
				return array(
					"error"  => $error,
					"result" => $log
				);
			}*/
		}


		$log = shell_exec("sudo systemctl status -n 12 ".SERVICE_MODEM);
		preg_match("/.*Active: (\w+) /i", $log, $matched );

		if ( !$error ) {
			
			if ( preg_match("/failed/i", $log) ) $error = "Modem error: view log for more details";
			else if ( $ctl=="start" ) {
				$timeout = 0; while( $timeout<30 ) {
					
					if ( preg_match("/local\s+ip address/i",$log) ) break;

					if ( preg_match("/modem hangup/i", $log) ) $error = "Modem hangup: check your configuration";
					if ( preg_match("/Connect script failed/i", $log) ) $error = "Connect script error";
					if ( preg_match("/input\/output error/i", $log) ) $error = "Modem error: input/output error";
					
					if ( $error ) {
						shell_exec("sudo /home/locusonus/bin/network.sh --modem stop 2>&1");
						break;
					}
					
					sleep(1); $timeout++;
					$log = shell_exec("sudo systemctl status -n 12 ".SERVICE_MODEM);
				}
			}
		}

		return array(
			"status" => netstatus("ppp0"),
			"error"  => $error,
			"active" => $matched[1]=="active" && !$error,
			"log" => $log
		);
	}

	function do_remote( $data )
	{
		$ini = parse_config();
		if ( $ini &&
			 $ini['REMOTE_ACCESS']!=$data['REMOTE_ACCESS'] || 
			 ($ini['REMOTE_ACCESS']=="yes" && $ini['REMOTE_ACCESS_HTTP_PORT']!=$data['REMOTE_ACCESS_HTTP_PORT']) || 
			 ($ini['REMOTE_ACCESS']=="yes" && $ini['REMOTE_ACCESS_SSH_PORT'] !=$data['REMOTE_ACCESS_SSH_PORT'])
		){
			$todo = $data['REMOTE_ACCESS']=="no" ? "stop" : "start";
			shell_exec( "sudo /home/locusonus/bin/network.sh --remote $todo" );
		}

	}

	function remote_port()
	{
		$data = parse_config();

		if ( $data['REMOTE_ACCESS']=="yes" ) {
			$status = shell_exec("sudo iptables -t nat -n -L");
			preg_match( "/.*tcp dpt:(\d+) redir ports 80/", $status, $HTTP );
			preg_match( "/.*tcp dpt:(\d+) redir ports 22/", $status, $SSH );
			return array(
				"http" => $HTTP[1],
				"ssh"  => $SSH[1],
			);
		}

		return false;
	}




	# -----------------------------------------------------------------------------------------------------------------
	#	SYSTEM
	# -----------------------------------------------------------------------------------------------------------------

	function do_loadfile( $file )
	{
		if ( !file_exists($file) ) die( "No such file or directory: $file" );
		if ( !is_readable($file) ) die( "Permission denied: $file" );

		print @file_get_contents($file);
	}

	function do_exec( $cmd, $quiet=false )
	{
		$cmd = escapeshellcmd($cmd);
		if ( $quiet ) exec( "$cmd &" );
		else system( $cmd.' 2>&1' );
	}

	function do_reboot()
	{
		do_stream("stop");
		system("sudo /usr/bin/systemctl reboot -i");
		sleep(10);
		print "reboot failed !";
	}

	function do_shutdown()
	{
		do_stream("stop");
		system("sudo /usr/bin/systemctl poweroff -i");
		sleep(10);
		print "shutdown failed !";
	}

	function do_service( $service, $enable )
	{
		$status = shell_exec("sudo /usr/bin/systemctl is-active $service");
		if ( $enable && trim($status)=="unknown" ) system("sudo /usr/bin/systemctl start $service" );
		if ( !$enable && trim($status)!="unknown" ) system("sudo /usr/bin/systemctl stop $service" );

		$status = shell_exec("sudo /usr/bin/systemctl is-enabled $service");
		if ( $enable && trim($status)!="enabled" ) system("sudo /usr/bin/systemctl enable $service" );
		if ( !$enable && trim($status)!="disabled" ) system("sudo /usr/bin/systemctl disable $service" );
	
	}

	function do_sysinfo()
	{

		$version = shell_exec( "grep -E \"^#\s+version:\" ".STREAMBOX_eCONFIG." | cut -d: -f2" );

		$cpu = shell_exec("cat /proc/cpuinfo");
		preg_match("/^model name\s+: (.*)/im", $cpu, $proc);
		preg_match("/^(ARMv\d+)/i", $proc[1], $arch);

		// http://elinux.org/RPi_HardwareHistory#Board_Revision_History
		$model = shell_exec("cat /proc/cpuinfo | grep 'Revision' | awk '{print $3}' | sed 's/^1000//'");
		switch( trim($model) ) {
			case "Beta"   : $name = "Model B (beta)"; break;
			case "0002"   : 
			case "0003"   : $name = "Model B Revision 1.0"; break;
			case "0004"   : 
			case "0005"   : 
			case "0006"   : $name = "Model B Revision 2.0"; break;
			case "0007"   : 
			case "0008"   : 
			case "0009"   : $name = "Model A Revision 2.0"; break;
			case "000d"   : 
			case "000e"   : 
			case "000f"   : $name = "Model B Revision 2.0"; break;
			case "0010"   : $name = "Model B+ Revision 1.0"; break;
			case "0011"   :
			case "0014"   : $name = "Compute Module 1 Revision 1.0"; break;
			case "0012"   : 
			case "0015"	  : $name = "Model A+ Revision 1.1"; break;
			case "0013"   : $name = "Model B+ Revision 1.2"; break;
			case "a01040" : $name = "2 Model B Revision 1.0"; break;
			case "a01041" : 
			case "a21041" : $name = "2 Model B Revision 1.1"; break;
			case "a22042" : $name = "2 Model B Revision 1.2"; break;
			case "900021" : $name = "Model A+ Revision 1.1"; break;
			case "900032" : $name = "Model B+ Revision 1.1"; break;
			case "900092" : $name = "Model Zero Revision 1.2"; break;
			case "900093" : 
			case "920093" : $name = "Model Zero Revision 1.3"; break;
			case "9000c1" : $name = "Model Zero W Revision 1.1"; break;
			case "a02082" : 
			case "a22082" : 
			case "a32082" : $name = "3 Model B Revision 1.2"; break;
			case "a020a0" : $name = "Compute Module 3 Revision 1.0"; break;
			case "a020d3" : $name = "3 Model B+ Revision 1.3"; break;


			case "9020e0" : $name = "Model 3 Model A+ Revision 1.0"; break;
			case "a02100" : $name = "Compute Module 3+ Revision 1.0"; break;
			case "a03111" :
			case "b03111" : 
			case "c03111" : $name = "Model 4 Model B Revision 1.1"; break;

			default: $name = "unknown (model revision: $model)"; break;
		}


		$mem = shell_exec("free -h");
		preg_match("/mem:\s+(\d+M)/Ui", $mem, $memory);

		printf(
			"streambox version ... %s (%s)".
			"\nnetwork node name ... %s".
			"\noperating system .... %s".
			"\nkernel .............. %s".
			"\nprocessor ........... %s".
			"\nboard version ....... %s".
			"\ntotal memory ........ %s".
			"\n",
			trim($version), strtolower($arch[1]),
			uname('n'), uname('o'), uname('srv'),
			$proc[1], $name, $memory[1]
		);
	}

	function uname( $opt )
	{
		return trim( shell_exec("uname -$opt") );
	}

?>
